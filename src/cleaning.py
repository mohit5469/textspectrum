import string
import emoji
import contractions
from spellchecker import SpellChecker
import re
class Datacleaning:
    def __init__(self, default_lang='en'):
        self.default_lang = default_lang
    
    def convert_emojis(self, text):
        return emoji.demojize(text)
    
    def convert_to_lowercase(self, text):
        return text.lower()
    
    def replace_contractions(self, text):
        text = text.replace("n't", " not")
        text = text.replace("'ve", " have")
        text = text.replace("'s", " is")
        text = contractions.fix(text)
        return text

    def remove_urls_n_email(self, text):
        text = re.sub(r'https?://\S+|www\.\S+', '', text) # Remove url's
        text = re.sub(r'\S+@\S+', '', text)  # Remove email addresses
        return text

    def handle_abbreviations(self, text):
        # Define your custom mapping of abbreviations and their expansions
        abbreviations = {
            'w/': 'with',
            'w/o': 'without',
            # 'u': 'you',
            # Add more abbreviations and expansions as needed
        }
        # Replace abbreviations with their expansions
        for abbreviation, expansion in abbreviations.items():
            text = text.replace(abbreviation, expansion)
        return text
    
    def handle_text_encoding(self, text, encoding='utf-8'):
        try:
            text = text.encode(encoding).decode(encoding)
        except UnicodeDecodeError:
            # Handle the encoding issue as per your requirements
            pass
        return text
    
    def remove_html_tags(self, text):
        return re.sub(r'<.*?>', '', text)
    
    def remove_punctuation(self, text):
        return text.translate(str.maketrans('', '', string.punctuation))
    
    def remove_special_characters(self, text):
        text = re.sub(r'[^a-zA-Z0-9\s]', '', text)
        return text
    
    def remove_numbers(self, text):
        text = re.sub(r'\d+', '', text)
        return text

    def remove_extra_spaces(self, text):
        return re.sub(r'\s+', ' ', text).strip()
    
    def handle_misspellings(self, text):
        spell = SpellChecker()
        words = text.split()
        corrected_words = []
        for word in words:
            corrected_word = spell.correction(word)
            if corrected_word is not None:
                corrected_words.append(corrected_word)
        corrected_text = ' '.join(corrected_words)
        return corrected_text

    def clean_text(self, text,text_cleaning):
        for cleaning_function in text_cleaning:
            if cleaning_function == 'convert_emojis':
                text = self.convert_emojis(text)
            elif cleaning_function == 'convert_to_lowercase':
                text = self.convert_to_lowercase(text)
            elif cleaning_function == 'replace_contractions':
                text = self.replace_contractions(text)
            elif cleaning_function == 'remove_urls_n_email':
                text = self.remove_urls_n_email(text)
            elif cleaning_function == 'handle_abbreviations':
                text = self.handle_abbreviations(text)
            elif cleaning_function == 'handle_text_encoding':
                text = self.handle_text_encoding(text)
            elif cleaning_function == 'remove_html_tags':
                text = self.remove_html_tags(text)
            elif cleaning_function == 'remove_punctuation':
                text = self.remove_punctuation(text)
            elif cleaning_function == 'remove_special_characters':
                text = self.remove_special_characters(text)
            elif cleaning_function == 'remove_numbers':
                text = self.remove_numbers(text)
            elif cleaning_function == 'remove_extra_spaces':
                text = self.remove_extra_spaces(text)
            elif cleaning_function == 'handle_misspellings':
                text = self.handle_misspellings(text)
        
        return text