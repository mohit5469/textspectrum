import requests
from PIL import Image
import streamlit as st

class homepage:
    def homepage():
        def load_lottieurl(url):
            r = requests.get(url)
            if r.status_code !=200: 
                return None
            return r.json()    

        # ---using local CSS---
        def local_css(file_name):
            with open(file_name) as f:
                st.markdown(f"<style>{f.read()}<\style>", unsafe_allow_html=True)

        local_css("src/style/style.css")
        #local_css(r"textspectrum\textspectrum\src\style\style.css")

        # --- Load Assets---
        lottie_about = load_lottieurl("https://assets8.lottiefiles.com/packages/lf20_LmW6VioIWc.json")
        #img_ourgoal = Image.open(r"textspectrum\textspectrum\src\visualization\MicrosoftTeams-image.png")
        img_ourgoal = Image.open(r"src/visualization/MicrosoftTeams-image.png")

        # ---Header Section---
        with st.container():
            st.subheader("Welcome to Textspectrum!")
            st.title("Unleash the ultimate NLP powerhouse")
            st.write("An all-encompassing NLP tool that enables seamless analysis of multilingual text with unprecedented ease, revolutionizing language understanding and unlocking limitless insights with a few simple clicks. TextSpectrum is a powerful solution that empowers users to perform text mining and processing tasks with just a few clicks.\n\nThe name \"TextSpectrum\" reflects its ability to cover a wide range of text analysis, from simple to complex. This versatile tool can be utilized in various industries such as retail, telecom, healthcare, government, and more, wherever user-generated content exists in the form of text. TextSpectrum is designed to cater to a diverse user base, including citizen data scientists, business teams, data analysts, and anyone who may not have expertise in coding or using numerous complex tools. With our user-friendly interface, anyone can leverage the capabilities of advanced text analysis without the need for extensive technical knowledge. Whether you need to extract valuable insights from customer reviews, analyze social media sentiments, or process large volumes of textual data, TextSpectrum simplifies the entire text analysis workflow. By utilizing our intuitive platform, users can effortlessly perform tasks such as data cleaning, text classification, entity recognition, sentiment analysis, and much more.\u200B")

        # ---What our app does---
        with st.container():
            st.write("---")
            left_column, right_column = st.columns(2)
            with left_column:
                st.header("Unleashing the Power: Our App Transforms Text with Style")
                st.write("With our user-friendly interface, anyone can leverage the capabilities of advanced text analysis without the need for extensive technical knowledge. Whether you need to extract valuable insights from customer reviews, analyze social media sentiments, or process large volumes of textual data, TextSpectrum simplifies the entire text analysis workflow. By utilizing our intuitive platform, users can effortlessly perform tasks such as data cleaning, text classification, entity recognition, sentiment analysis, and much more.​")

        with right_column:
            st.lottie(lottie_about, height= 300, key= "about")

        # ---Our main page---
        with st.container():
            st.write("---")
            st.header("Our Goal")
            st.write("##")
            image_column, text_column = st.columns((1,2))
            with image_column:
                st.image(img_ourgoal, use_column_width=True)

            with text_column:
                st.write(
                    """
                    We understand that time is of the essence, and our goal is to provide you with a seamless and efficient text analysis experience. \n
                    By eliminating the need for manual coding and complex toolchains, TextSpectrum enables you to save valuable time and resources, allowing you to focus on deriving meaningful insights and making informed decisions.

                    Get Started
                    """
                )    
                st.markdown("[Sign Up/ Login]")

        # ---Contact Us---
        with st.container():
            st.write("---")       
            st.header("Get In Touch With Us") 
            st.write("##")

            # Documention: https://formsubmit.co/ !!! CHANGE EMAIL ADDRESS !!!
            Contact_form = """
            <form action="https://formsubmit.co/sanjay.s@vcreatek.com" method="POST">
                <input type="hidden" name="_captcha" value="false">
                <input type="text" name="name" placeholder="Your name" required>
                <input type="email" name="email" placeholder="Your email" required>
                <textarea name="message" placeholder="Your message here" required></textarea>
                <button type="submit">Send</button>
            </form> 
            """
            left_column, right_column = st.columns(2)
            with left_column:
                st.markdown(Contact_form, unsafe_allow_html=True)
            with right_column:
                st.empty()   