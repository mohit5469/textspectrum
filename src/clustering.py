import pandas as pd
import numpy as np
import ast
import openai
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
import streamlit as st
import os

class ClusterAnalyzer:
    
    def __init__(self, api_key):
        # Initialize the AzureOpenAI client
        self.client = api_key=api_key
            #azure_endpoint=endpoint
        
    
    def preprocess_data(self, df):
        df['embeddings'] = df['embeddings'].astype(str)
        df = df.drop_duplicates(subset=['embeddings'])
        df['embeddings'] = df['embeddings'].apply(lambda x: ast.literal_eval(x) if isinstance(x, str) else x)  
        print(df['embeddings'])
        df['embeddings'] = df['embeddings'].apply(lambda x: np.array(x) if isinstance(x, list) else x)
        df = df[df['embeddings'].notna()]
        df = df[df['embeddings'].apply(lambda x: x.shape[0] == 1536)]
        return df


    
    def determine_optimal_clusters(self, embeddings):
        range_n_clusters = list(range(2,15))
        best_num_clusters = 1
        best_silhouette = -1
        for n_clusters in range_n_clusters:
            clusterer = KMeans(n_clusters=n_clusters, random_state=42)
            cluster_labels = clusterer.fit_predict(embeddings)
            silhouette_avg = silhouette_score(embeddings, cluster_labels)
            if silhouette_avg > best_silhouette:
                best_silhouette = silhouette_avg
                best_num_clusters = n_clusters
        return best_num_clusters
    
    def cluster_data(self, df):
        embeddings = np.vstack(df['embeddings'].values)
        best_num_clusters = self.determine_optimal_clusters(embeddings)
        kmeans = KMeans(n_clusters=best_num_clusters, random_state=42)
        df['cluster'] = kmeans.fit_predict(embeddings)
        return df, best_num_clusters

    # def assign_cluster_names(self, df, best_num_clusters):

    #     cluster_name_dict = {}
    #     prompt = 'Based on the following complaints, please suggest a name for this cluster that encapsulates the primary issues or problems mentioned.'
    #     for i in range(best_num_clusters):
    #         sample_reviews = df[df['cluster'] == i].PROD_FBACK_DESC.sample(5, random_state=42).values
    #         complaints = "\n".join(sample_reviews)
    #         cluster_name = openai.ChatCompletion.create(
    #             engine="Chat",
    #             messages=[
    #                 {"role": "system", "content": prompt},
    #                 {"role": "user", "content": complaints}
    #             ]
    #         )['choices'][0]['message']['content']
    #         cleaned_cluster_name = cluster_name.replace("Product Problem Cluster: ", "").strip()
    #         cluster_name_dict[i] = cleaned_cluster_name
    #     df['cluster_name'] = df['cluster'].map(cluster_name_dict)
    #     return df

    def assign_cluster_names(self, df, best_num_clusters,selected_cols_topic_modelling):
        cluster_name_dict = {}
        prompt_template = 'Based on the following complaints, please suggest a name for this cluster that encapsulates the primary issues or problems mentioned.'

        for i in range(best_num_clusters):
            if "PREPROCESSED_TEXT" in st.session_state.model_df:
                sample_reviews = df[df['cluster'] == i].PREPROCESSED_TEXT.sample(5, random_state=42).values
            else:
                 sample_reviews = df[df['cluster'] == i][selected_cols_topic_modelling[0]].sample(5, random_state=42).values   
            complaints = "\n".join(sample_reviews)
            prompt = f"{prompt_template}\n\nComplaints:\n{complaints}\n\nSuggested Cluster Name:"

            # Generate the cluster name using the AzureOpenAI client
            response = openai.ChatCompletion.create(
                #model="Chat",  # Use your specific deployment name if different
                model="gpt-3.5-turbo-16k",
                messages=[
                    {"role": "system", "content": prompt_template},
                    {"role": "user", "content": complaints}
                ]
            )

            cluster_name = response.choices[0].message.content.strip()
            cleaned_cluster_name = cluster_name.replace("Product Problem Cluster: ", "").strip()
            cluster_name_dict[i] = cleaned_cluster_name

        df['cluster_name'] = df['cluster'].map(cluster_name_dict)
        return df



    def visualize_clusters(self, df, embeddings, cluster_name_dict):
        tsne = TSNE(n_components=2, perplexity=15, random_state=41, init="random", learning_rate=200)
        vis_dims2 = tsne.fit_transform(embeddings)
    
        x = [x for x, y in vis_dims2]
        y = [y for x, y in vis_dims2]
        z = np.random.rand(len(x))
    
        fig=plt.figure(figsize=(15, 15))
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)


        for category, color in enumerate(["purple", "green", "red", "blue", "yellow", "orange", 'black', 'brown', 'grey', 'olive', 'cyan', 'pink']):
            xs = np.array(x)[df.cluster == category]
            ys = np.array(y)[df.cluster == category]
            zs = np.array(z)[df.cluster == category]
            
            plt.scatter(xs, ys, s=zs*1000, color=color, alpha=0.3)

            avg_x = xs.mean()
            avg_y = ys.mean()

            plt.scatter(avg_x, avg_y, marker="x", color=color, s=100)
            cluster_name = cluster_name_dict.get(category, f"Cluster {category}")
            plt.annotate(cluster_name, (avg_x, avg_y), textcoords="offset points", xytext=(0,10), ha='center', fontsize=20)
    
        plt.title("Clusters identified", fontsize=16)
        #plt.show()
        st.pyplot(fig)


    def analyze_and_visualize(self, df,selected_cols_topic_modelling):
        embeddings = np.vstack(df['embeddings'].values)
        #df = self.preprocess_data(df)
        df, best_num_clusters = self.cluster_data(df)
        df = self.assign_cluster_names(df, best_num_clusters,selected_cols_topic_modelling)
        st.write(df)
        # embeddings = np.vstack(df['embeddings'].values)
        self.visualize_clusters(df, embeddings, dict(zip(df['cluster'], df['cluster_name'])))
        return df