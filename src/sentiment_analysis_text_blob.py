from textblob import TextBlob

#TextBlob Sentiment analysis
class text_blob_sentiment_analysis:
    def text_blob_sentiment_analysis(df, col_name):
        def getSubjectivity(text):
            return TextBlob(text).sentiment.subjectivity
                                                                                                                                                                
        #Create a function to get the polarity
        def getPolarity(text):
            return TextBlob(text).sentiment.polarity

        #Create two new columns ‘Subjectivity’ & ‘Polarity’
        #df['TextBlob_Subjectivity'] = df['REVIEW_CONTENT'].apply(getSubjectivity)
        df ['textBlob_score'] = df[col_name].apply(getPolarity)
        
        def getAnalysis(score):
            if score < 0:
                return 'Negative'
            elif score == 0:
                return 'Neutral'
            else:
                return 'Positive'
        df['TextBlob_sentiment'] = df['textBlob_score'].apply(getAnalysis)
        
        return df