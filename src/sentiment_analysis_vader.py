#Vader Sentiment Analysis imports
import pandas as pd
import nltk
from nltk.sentiment.vader import SentimentIntensityAnalyzer
nltk.download('vader_lexicon')

#Vader Sentiment Analysis
class class_vader_sentiment_analysis:
	def vader_sentiment_analysis(df, col_name):
		sent_i = SentimentIntensityAnalyzer()
		
		def vader_sentiment_scores(text):
			""" Calculate and return the nltk vader (lexicon method) sentiment """
			return sent_i.polarity_scores(text)['compound']

		def categorise_sentiment(sentiment, neg_threshold=-0.05, pos_threshold=0.05):
			""" categorise the sentiment value as positive (1), negative (-1) 
				or neutral (0) based on given thresholds """
			if sentiment < neg_threshold:
				label = 'negative'
			elif sentiment > pos_threshold:
				label = 'positive'
			else:
				label = 'neutral'
			return label
		
		# create new column for vader compound sentiment score
		df['vader_score'] = df[col_name].apply(vader_sentiment_scores)
		# new col with vader sentiment label based on vader compound score
		df['vader_sentiment'] = df['vader_score'].apply(categorise_sentiment)

		return df