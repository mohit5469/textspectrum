import openai
import pandas as pd
import streamlit as st
from openpyxl import load_workbook,Workbook,worksheet
import re
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
# import tabula
# from tabula import read_pdf
import plotly.express as px
import pygwalker as pyg
import string
import emoji
import contractions
from spellchecker import SpellChecker
import numpy as np
import requests
from streamlit_lottie import st_lottie  
from tkinter import *
from PIL import Image
import wordcloud
from wordcloud import WordCloud,STOPWORDS, ImageColorGenerator
from subprocess import call 
import sqlalchemy as sa
import urllib
import pyodbc
from sqlalchemy.orm import scoped_session,sessionmaker
from sqlalchemy import create_engine,inspect
from joblib import Parallel, delayed
from tqdm import tqdm
import nltk
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer, WordNetLemmatizer
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk import pos_tag
from gensim.models import Word2Vec, KeyedVectors
from gensim.models import FastText
import spacy
import en_core_web_sm
#import spacy.cli
#spacy.cli.download("en_core_web_sm")
#from transformers import pipeline
from st_aggrid import AgGrid
from st_aggrid.grid_options_builder import GridOptionsBuilder
import sys
sys.setrecursionlimit(1500)
import streamlit.components.v1 as components
# from spellchecker import SpellChecker
import time
from dotenv import load_dotenv
import dotenv
from sklearn.metrics import silhouette_score
import ast
from sklearn.cluster import KMeans
from sklearn.manifold import TSNE
from sentence_transformers import SentenceTransformer

config = dotenv.dotenv_values(dotenv_path=r"C:\Users\AdityaSingh\textspectrum\textspectrum\src\config.env")
openai.api_key = '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' #config['OPENAI_API_KEY']
page_title = "TextSpectrum"
page_icon = "broom"  # Replace with the URL of the image


# Configure the Streamlit page
st.set_page_config(
    page_title=page_title,
    page_icon=page_icon,
    layout="wide")
st.sidebar.markdown("<h1 style='text-align: center; color: red;'>Textspectrum!!</h1>", unsafe_allow_html=True) 
st.set_option('deprecation.showPyplotGlobalUse', False)

uploaded_file = st.sidebar.file_uploader("📂 Connect to data")
button1= st.sidebar.button("Connect To SQL Database")
options = st.sidebar.radio('Select what you want to display:', ['Home','View Data & Summary' , 'Data Cleaning','Text Pre-Processing', 'Sentiment Analysis', 'Key Phrase Extraction','Text Classification Model','Topic Modelling','Summarization','Semantic Search','Knowledge Graph','EDA'])
def convert_df(df):
    # IMPORTANT: Cache the conversion to prevent computation on every rerun
    return df.to_csv(index=False).encode('utf-8')
    

def home(uploaded_file):
    if uploaded_file:
        st.write(st.session_state.df)
        st.header('Statistics of Dataframe')
        st.write(st.session_state.df.describe()) 

    elif st.session_state['button'] == True :
        st.write(st.session_state.df)
        st.header('Statistics of Dataframe')
        st.write(st.session_state.df.describe())     
         
    else:
        st.header('Load Your data to get started')
    
class TextPreprocessor:
            def __init__(self):
                nltk.download('punkt')
                nltk.download('stopwords')
                nltk.download('averaged_perceptron_tagger')
                nltk.download('maxent_ne_chunker')
                nltk.download('words')
                nltk.download('wordnet')

            def tokenize_sentences(self, text):
                return sent_tokenize(text)
            
            def tokenize_words(self, text):
                return word_tokenize(text)
            
            def remove_stopwords(self, text):
                stop_words = set(stopwords.words('english'))
                words = word_tokenize(text)
                filtered_words = [word for word in words if word.casefold() not in stop_words]
                return ' '.join(filtered_words)
            
            def perform_stemming(self, text):
                stemmer = PorterStemmer()
                words = word_tokenize(text)
                stemmed_words = [stemmer.stem(word) for word in words]
                return ' '.join(stemmed_words)
            
            def perform_lemmatization(self, text):
                lemmatizer = WordNetLemmatizer()
                words = word_tokenize(text)
                lemmatized_words = [lemmatizer.lemmatize(word) for word in words]
                return ' '.join(lemmatized_words)
            
            def perform_pos_tagging(self, text):
                    words = word_tokenize(text)
                    pos_tags = pos_tag(words)
                    pos_dict = {word: tag for word, tag in pos_tags}
                    return pos_dict
            
            def create_bag_of_words(self, text, output_format):
                words = text.split()
                bag_of_words = pd.Series(words).value_counts().to_dict()
                if  'dictionary' in output_format:
                    return bag_of_words
                elif 'sparse_matrix' in output_format:
                    return bag_of_words
                else:
                    raise ValueError("Invalid output_format. Use 'matrix' or 'dict'.")
                
            def perform_ner(self, text, selected_entities):
                    entities = []
                    if "basic" in selected_entities:
                        doc_spacy = nlp_spacy(text)
                        entities.extend([(ent.label_, ent.text) for ent in doc_spacy.ents])

                    if "person" in selected_entities:
                        doc_spacy = nlp_spacy(text)
                        entities.extend([(ent.label_, ent.text) for ent in doc_spacy.ents if ent.label_ == "PERSON"])

                    if "organization" in selected_entities:
                        doc_spacy = nlp_spacy(text)
                        entities.extend([(ent.label_, ent.text) for ent in doc_spacy.ents if ent.label_ == "ORG"])

                    if "location" in selected_entities:
                        doc_spacy = nlp_spacy(text)
                        entities.extend([(ent.label_, ent.text) for ent in doc_spacy.ents if ent.label_ in ["GPE", "LOC", "FAC"]])

                    return entities
        
            def preprocess_text(self, text, selected_entities, output_format):
                for pre_process in text_pre_process:
                    if pre_process == 'tokenize_sentences':
                        text = self.tokenize_sentences(str(text))
                    elif pre_process == 'tokenize_words':    
                        text = self.tokenize_words(str(text))
                    elif pre_process == 'remove_stopwords':
                        text = self.remove_stopwords(str(text))
                    elif pre_process == 'perform_stemming':
                        text = self.perform_stemming(str(text))
                    elif pre_process == 'perform_lemmatization':
                        text = self.perform_lemmatization(str(text))
                    elif pre_process == 'perform_pos_tagging':
                        text = self.perform_pos_tagging(str(text))
                    elif pre_process == 'create_bag_of_words':
                        bag_of_words = self.create_bag_of_words(str(text), output_format)
                    elif pre_process == 'perform_ner':
                        ner_entities = self.perform_ner(str(text), selected_entities)       
                return text, bag_of_words, ner_entities 
            
            def preprocess_text_1(self, text, output_format):
                for pre_process in text_pre_process:
                    if pre_process == 'tokenize_sentences':
                        text = self.tokenize_sentences(str(text))
                    elif pre_process == 'tokenize_words':    
                        text = self.tokenize_words(str(text))
                    elif pre_process == 'remove_stopwords':
                        text = self.remove_stopwords(str(text))
                    elif pre_process == 'perform_stemming':
                        text = self.perform_stemming(str(text))
                    elif pre_process == 'perform_lemmatization':
                        text = self.perform_lemmatization(str(text))
                    elif pre_process == 'perform_pos_tagging':
                        text = self.perform_pos_tagging(str(text))
                    elif pre_process == 'create_bag_of_words':
                        bag_of_words = self.create_bag_of_words(str(text), output_format)      
                return text, bag_of_words 
            
            def preprocess_text_2(self, text, selected_entities):
                for pre_process in text_pre_process:
                    if pre_process == 'tokenize_sentences':
                        text = self.tokenize_sentences(text)
                    elif pre_process == 'tokenize_words':    
                        text = self.tokenize_words(text)
                    elif pre_process == 'remove_stopwords':
                        text = self.remove_stopwords(str(text))
                    elif pre_process == 'perform_stemming':
                        text = self.perform_stemming(str(text))
                    elif pre_process == 'perform_lemmatization':
                        text = self.perform_lemmatization(str(text))
                    elif pre_process == 'perform_pos_tagging':
                        text = self.perform_pos_tagging((text))
                    elif pre_process == 'perform_ner':
                        ner_entities = self.perform_ner(str(text), selected_entities)         
                return text, ner_entities 
            
            def preprocess_text_3(self, text):
                for pre_process in text_pre_process:
                    if pre_process == 'tokenize_sentences':
                        text = self.tokenize_sentences(text)
                    elif pre_process == 'tokenize_words':    
                        text = self.tokenize_words(text)
                    elif pre_process == 'remove_stopwords':
                        text = self.remove_stopwords(str(text))
                    elif pre_process == 'perform_stemming':
                        text = self.perform_stemming(text)
                    elif pre_process == 'perform_lemmatization':
                        text = self.perform_lemmatization(text)
                    elif pre_process == 'perform_pos_tagging':
                        text = self.perform_pos_tagging(text)       
                return text 
            
            def preprocess_text_4(self, text ,selected_entities, output_format):
                for pre_process in text_pre_process:
                    if pre_process == 'tokenize_sentences':
                        text = self.tokenize_sentences(str(text))
                    elif pre_process == 'tokenize_words':    
                        text = self.tokenize_words(str(text))
                    elif pre_process == 'remove_stopwords':
                        text = self.remove_stopwords(str(text))
                    elif pre_process == 'perform_stemming':
                        text = self.perform_stemming(str(text))
                    elif pre_process == 'perform_lemmatization':
                        text = self.perform_lemmatization(str(text))
                    elif pre_process == 'perform_pos_tagging':
                        pos_tagging = self.perform_pos_tagging(str(text))
                    elif pre_process == 'create_bag_of_words':
                        bag_of_words = self.create_bag_of_words(str(text), output_format)
                    elif pre_process == 'perform_ner':
                        ner_entities = self.perform_ner(str(text), selected_entities)       
                return text,pos_tagging ,bag_of_words, ner_entities 
            
            def preprocess_text_5(self, text,output_format):
                for pre_process in text_pre_process:
                    if pre_process == 'tokenize_sentences':
                        text = self.tokenize_sentences(str(text))
                    elif pre_process == 'tokenize_words':    
                        text = self.tokenize_words(str(text))
                    elif pre_process == 'remove_stopwords':
                        text = self.remove_stopwords(str(text))
                    elif pre_process == 'perform_stemming':
                        text = self.perform_stemming(str(text))
                    elif pre_process == 'perform_lemmatization':
                        text = self.perform_lemmatization(str(text))
                    elif pre_process == 'perform_pos_tagging':
                        pos_tagging = self.perform_pos_tagging(str(text))
                    elif pre_process == 'create_bag_of_words':
                        bag_of_words = self.create_bag_of_words(str(text), output_format)      
                return text,pos_tagging ,bag_of_words 
            
            def preprocess_text_6(self, text,selected_entities):
                for pre_process in text_pre_process:
                    if pre_process == 'tokenize_sentences':
                        text = self.tokenize_sentences(text)
                    elif pre_process == 'tokenize_words':    
                        text = self.tokenize_words(text)
                    elif pre_process == 'remove_stopwords':
                        text = self.remove_stopwords(str(text))
                    elif pre_process == 'perform_stemming':
                        text = self.perform_stemming(str(text))
                    elif pre_process == 'perform_lemmatization':
                        text = self.perform_lemmatization(str(text))
                    elif pre_process == 'perform_pos_tagging':
                        pos_tagging = self.perform_pos_tagging((text))
                    elif pre_process == 'perform_ner':
                        ner_entities = self.perform_ner(str(text), selected_entities)         
                return text, pos_tagging,ner_entities
            
            def preprocess_text_7(self, text):
                for pre_process in text_pre_process:
                    if pre_process == 'tokenize_sentences':
                        text = self.tokenize_sentences(text)
                    elif pre_process == 'tokenize_words':    
                        text = self.tokenize_words(text)
                    elif pre_process == 'remove_stopwords':
                        text = self.remove_stopwords(str(text))
                    elif pre_process == 'perform_stemming':
                        text = self.perform_stemming(str(text))
                    elif pre_process == 'perform_lemmatization':
                        text = self.perform_lemmatization(str(text))
                    elif pre_process == 'perform_pos_tagging':
                        pos_tagging = self.perform_pos_tagging((text))     
                return text, pos_tagging
            
def get_word_embeddings(reviews, embedding_technique):
    if embedding_technique == "word2vec":
        # Tokenize the reviews into words
        tokenized_reviews = [word_tokenize(review.lower()) for review in reviews]
        # Train Word2Vec model
        model = Word2Vec(sentences=tokenized_reviews, vector_size=100, window=5, min_count=1, sg=0)
        # Compute average word embeddings for each review
        review_embeddings = []
        for review_tokens in tokenized_reviews:
            embeddings = [model.wv[token] for token in review_tokens if token in model.wv]
            avg_embedding = sum(embeddings) / len(embeddings) if embeddings else None
            review_embeddings.append(avg_embedding)
 
    elif embedding_technique == "glove":
        # Load the pre-trained GloVe Word2Vec model
        glove_model = KeyedVectors.load_word2vec_format(r"C:\Users\qahmed\Documents\test\glove.6B.100d.word2vec", binary=False)
 
        # Tokenize the reviews into words
        tokenized_reviews = [word_tokenize(review.lower()) for review in reviews]
 
        # Compute average word embeddings for each review using GloVe
        review_embeddings = []
        for review_tokens in tokenized_reviews:
            embeddings = [glove_model[token] for token in review_tokens if token in glove_model]
            avg_embedding = sum(embeddings) / len(embeddings) if embeddings else None
            review_embeddings.append(avg_embedding)
 
    elif embedding_technique == "fasttext":
        # Tokenize the reviews into sentences
        tokenized_reviews = [word_tokenize(review.lower()) for review in reviews]
        # Train FastText model
        model = FastText(sentences=tokenized_reviews, vector_size=100, window=5, min_count=1, sg=0)
        # Compute sentence embeddings
        review_embeddings = [model.wv[sent_tokens].mean(axis=0) for sent_tokens in tokenized_reviews]
 
    else:
        raise ValueError("Invalid embedding technique choice.")
 
    return review_embeddings

def get_sentence_embeddings(reviews, embedding_technique):
    # Initialize the model based on user's choice
    if embedding_technique == 'MiniLM':
        model = SentenceTransformer('paraphrase-MiniLM-L6-v2')

    elif embedding_technique == 'DistilRoBERTa':
        model = SentenceTransformer('paraphrase-distilroberta-base-v2')

    elif embedding_technique == 'RoBERTa':
        model = SentenceTransformer('stsb-roberta-large')

    else:
        st.write("Invalid model choice. Please choose from 'MiniLM', 'DistilRoBERTa', or 'RoBERTa'")
        return None
    review_embeddings = model.encode(reviews)
    # review_embeddings=pd.Series(review_embeddings)
    # st.session_state.pre_process_df['embeddings'] =list(review_embeddings)
    if "pre_process_df" in st.session_state:
        subset_df = st.session_state.pre_process_df.iloc[:len(review_embeddings)]
    if  "model_df" in st.session_state:  
         subset_df = st.session_state.model_df.iloc[:len(review_embeddings)]  
    subset_df['embeddings'] = list(review_embeddings)
    st.session_state.pre_process_df = subset_df
    return st.session_state.pre_process_df  

          
            
def data_summary():
        st.header('Statistics of Dataframe')
        st.write(st.session_state.df.describe()) 

def space(num_lines=1):
    for _ in range(num_lines):
        st.write("")

def sidebar_space(num_lines=1):
    for _ in range(num_lines):
        st.sidebar.write("")


def sidebar_multiselect_container(massage, arr, key):    
    container = st.sidebar.container()
    select_all_button = st.sidebar.checkbox("Select all for " + key)
    if select_all_button:
                selected_num_cols = container.multiselect(massage, arr, default = list(arr))                 
    else:
         selected_num_cols = container.multiselect(massage, arr)

    return selected_num_cols  

def Word_Cloud(df, select_col_for_wordcloud):
    try:
        text = " ".join(str(word) for word in st.session_state.df_viz[select_col_for_wordcloud])
        stopwords = set(STOPWORDS)
        wordcloud = WordCloud(stopwords=stopwords, background_color="white",include_numbers=True).generate(text)
        plt.figure( figsize=(15,10),edgecolor="black")
        plt.imshow(wordcloud, interpolation='bilinear')
        plt.axis(True)
        return plt.show()

    except:
        html_output = "Please select the String column"
        st.write(html_output)
        return html_output
    
def prep_data_for_wordCloud(df, select_col_for_wordcloud):
            textSeries = df[select_col_for_wordcloud].dropna()
            textDF = pd.DataFrame(textSeries)
            return textDF    

def dashboard():
    # all_vizuals = [ 'Descriptive Analysis', 'Target Analysis', 
    #                  'Count Plots of Categorical Columns', 
    #                 'Box Plots', 'Outlier Analysis','Line Chart','Bar Chart','APP']
    all_vizuals = ['EDA','WordCloud']  
    sidebar_space(3)         
    vizuals = st.sidebar.multiselect("Choose which visualizations you want to see 👇", all_vizuals,key='EDA')     

    if 'df_viz' not in st.session_state:
        st.session_state.df_viz = st.session_state.df 
    
    if 'EDA' in vizuals:
                html_output = pyg.walk(st.session_state.df_viz, env='Streamlit')
                st.write(html_output, unsafe_allow_html=True)

    if 'WordCloud' in vizuals:
        select_col_for_wordcloud = st.selectbox('Choose Column for WordCloud:', st.session_state.df_viz.columns)
        #df =  st.session_state.df
        prep_data_for_wordCloud(st.session_state.df_viz, select_col_for_wordcloud)
        html_output = Word_Cloud(st.session_state.df_viz, select_col_for_wordcloud)
        st.set_option('deprecation.showPyplotGlobalUse', False)
        #print(html_output)
        st.pyplot(html_output,clear_figure=True)    

def get_table_from_sql():
                    df=pd.read_sql_table(table_name=st.session_state["tablename"],con=st.session_state["engine"])
                    df1=df
                    if "df" not in st.session_state:
                        st.session_state["df"]=df 
                    st.session_state["df"] = df  
                    #st.write(st.session_state.df)
                    return df1    
           
def db_auth():
    placeholder = st.empty()
    with placeholder.container():
        col1, col2, col3 = st.columns(3)
        with col1:
            st.markdown("<h1 style='text-align: center; color: red;'>Connect To SQL DB!!</h1>", unsafe_allow_html=True)
            user = st.text_input("Username")
            password = st.text_input("Password",type="password")
            host = st.text_input("Host/Server")
            database = st.text_input("Database")  
            button2= st.button("Connect")   
            if st.session_state.get('button2') != True:
                st.session_state['button2'] = button2              
        if st.session_state["button2"] == True: 
            try:
                conn= urllib.parse.quote_plus('DRIVER={SQL Server};SERVER='+host+';DATABASE='+database+';UID='+user+';PWD='+ password)
                engine = sa.create_engine('mssql+pyodbc:///?odbc_connect={}'.format(conn),use_setinputsizes=False)
                st.write(
                                f"Connection to the {host} for user {user} created successfully.")
                inspection = inspect(engine)
                all_tables = inspection.get_table_names()
                # st.write(all_tables)
                                # all_tables1 = ' ', inspection.get_table_names()
                                # st.write(all_tables1)
                tablename = st.selectbox("Select Table Name",options=all_tables)
                if "tablename" not in st.session_state:
                            st.session_state["tablename"]=tablename
                if "engine" not in st.session_state:
                            st.session_state["engine"]=engine   
                # st.write(tablename)
                st.session_state["tablename"]=tablename  
                # st.write(st.session_state)            
                get_table_from_sql() 
                st.session_state["tablename"]=tablename
            except :
                col1, col2, col3 = st.columns(3)
                with col2:
                        st.write("**Authentication error: reload the page**") 

if uploaded_file is not None:
    if uploaded_file.type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
            df = pd.read_excel(uploaded_file,engine='openpyxl') 
            df1 =df
            if "df" not in st.session_state:
                st.session_state["df"]=df 

    if uploaded_file.type == 'text/csv':
            df = pd.read_csv(uploaded_file,encoding='latin-1') 
            df1=df
            if "df" not in st.session_state:
                    st.session_state["df"]=df
               
# if  uploaded_file is None and st.session_state['button']== True:   
#     df1= db_auth()
            df = pd.read_csv(uploaded_file) 
            df1=df
            if "df" not in st.session_state:
                    st.session_state["df"]=df
               
# if  uploaded_file is None and st.session_state['button']== True:   
#     df1= db_auth()
    # if uploaded_file.type == 'application/pdf':
    #     from tabula import read_pdf
    #     import tabula
    #     df = read_pdf(uploaded_file,pages="all")
    #     tabula.convert_into(uploaded_file, "df3.csv", output_format="csv", pages='all')
    #     df2 = pd.read_csv("df3.csv",encoding='cp1252')
             
if options == 'Home':
    if st.session_state.get('button') != True:
        st.session_state['button'] = button1       

    if st.session_state['button']== False:
         import homepage
         from homepage import homepage
         homepage.homepage()

    if st.session_state['button'] == True:
        db_auth()
        if st.session_state['button2'] == True:
            gb = GridOptionsBuilder.from_dataframe(st.session_state.df)

            gb.configure_pagination()
            gb.configure_side_bar()
            gb.configure_default_column(groupable=True, value=True, enableRowGroup=True, aggFunc="sum", editable=True)
            gridOptions = gb.build()

            AgGrid(st.session_state.df, gridOptions=gridOptions, enable_enterprise_modules=True)
            #st.write(st.session_state.df)

elif options == 'View Data & Summary':    
    home(uploaded_file)
         
elif options == 'Data Cleaning':
    if uploaded_file is not None or st.session_state['button'] == True :
        import cleaning
        from cleaning import Datacleaning
        all_text_process = ['data_subset','convert_emojis','convert_to_lowercase','replace_contractions','remove_urls_n_email','handle_abbreviations','handle_text_encoding','remove_html_tags','remove_punctuation','remove_special_characters',
                        'remove_numbers','remove_extra_spaces','handle_misspellings']    
        sidebar_space(3) 
        if  uploaded_file is None and st.session_state['button'] == True :
            df1=get_table_from_sql()
            df=pd.DataFrame(df1)
        text_cleaning = st.multiselect("Choose text cleaning functions",all_text_process)
        if  'data_subset' in text_cleaning: 
            subset_col=st.multiselect('Choose Columns:', df.columns, key='Select Columns_01')
            sample_fraction = st.slider('Select Sample Fraction', 0.0, 1.0, 0.1, 0.01)
            if st.button("Run Data Subset"):
                if subset_col is None or len(subset_col) == 0:
                    sub_df = df.sample(frac=sample_fraction)
                    
                else:
                    sub_df = df[subset_col].sample(frac=sample_fraction)

                if "sub_df" not in st.session_state:
                    st.session_state.sub_df=sub_df
                    st.session_state.df=st.session_state.sub_df

                if "sub_df" in st.session_state:
                     st.write(st.session_state.sub_df)   
        else:
            if "sub_df" in st.session_state:   
                selected_cols_clean = st.multiselect('Choose Column for Text Cleansing:', st.session_state.sub_df.select_dtypes(include = 'object').columns, key='Text ') 
            else :
                selected_cols_clean = st.multiselect('Choose Column for Text Cleansing:', df.select_dtypes(include = 'object').columns, key='Text ')    
            if st.button("Run Data Cleaning"):    
                        preprocessor = Datacleaning()
                        st.session_state.df.dropna(subset=selected_cols_clean[0], inplace=True)
                        st.session_state.df.reset_index(drop=True, inplace=True)
                        #preprocessed_reviews = st.session_state.df[selected_cols_clean[i]].apply(preprocessor.clean_text(text_cleaning=text_cleaning))
                        preprocessed_reviews = st.session_state.df[selected_cols_clean[0]].apply(lambda x: preprocessor.clean_text(x, text_cleaning=text_cleaning))
                    # # Add the preprocessed reviews as a new column in the DataFrame
                        clean_df=pd.DataFrame(st.session_state.df)
                        if "clean_df" in st.session_state:
                            st.session_state.clean_df=clean_df

                        if "clean_df" not in st.session_state:
                            st.session_state.clean_df=clean_df

                        st.session_state.clean_df['Cleaned Text for '+ selected_cols_clean[0]] = preprocessed_reviews              
                #st.write(st.session_state.df) 
                     
                        if "clean_df" in st.session_state:
                                gb = GridOptionsBuilder.from_dataframe(st.session_state.clean_df)

                                gb.configure_pagination()
                                gb.configure_side_bar()
                                gb.configure_default_column(groupable=True, value=True, enableRowGroup=True, aggFunc="sum", editable=True)
                                gridOptions = gb.build()

                                AgGrid(st.session_state.clean_df, gridOptions=gridOptions, enable_enterprise_modules=True)
                                if "df_viz" not in st.session_state:
                                    st.session_state.df_viz=st.session_state.clean_df

                                if "df_viz" in st.session_state:
                                    st.session_state.df_viz=st.session_state.clean_df   
                                csv = convert_df(st.session_state.clean_df)
                                st.download_button(
                                                label="Download data as CSV",
                                                data=csv,
                                                file_name='clean_data.csv',
                                                mime='text/csv',
                                            )           
                
    else :
         st.header('Load Your data to get started')

elif options=='Text Pre-Processing':
            if uploaded_file is not None or st.session_state['button'] == True:
                all_text_pre_process = ['tokenize_sentences','remove_stopwords','perform_stemming','perform_lemmatization','perform_pos_tagging','create_bag_of_words','perform_ner','embeddings']
                sidebar_space(3)
                if "clean_df" not in st.session_state and "sampled_df" not in st.session_state :     
                    st.session_state.clean_df=st.session_state.df 
                if  uploaded_file is None and st.session_state['button'] == True :
                    df=get_table_from_sql()
                    st.session_state.clean_df=pd.DataFrame(df)

                if "clean_df" not in st.session_state and "sampled_df" in  st.session_state :     
                    st.session_state.clean_df=st.session_state.sampled_df         

                text_pre_process = st.multiselect("Choose Text Pre Processing functions",all_text_pre_process)
                nlp_spacy=en_core_web_sm.load()
                #nlp_spacy = spacy.load("en_core_web_sm")
                selected_cols_pre_process = st.multiselect('Choose Column for Text Pre Processing:', st.session_state.clean_df.select_dtypes(include = 'object').columns, key='Text_PreProcessing') 
                #preprocessor = TextPreprocessor()
                if 'create_bag_of_words' in text_pre_process and 'perform_ner' in text_pre_process:
                    all_entities = ('person','organization','location','basic')
                    selected_entities=st.multiselect("Select Entities for NER",options=all_entities)
                    all_op_format=['sparse_matrix','dictionary']
                    output_format = st.multiselect("Choose the output format (sparse_matrix/dictionary) for bag of words",options=all_op_format)  
                    i = 0
                    while (i < len(selected_cols_pre_process) and len(text_pre_process)>= 1 and len(output_format)>=1 and len(selected_entities)>=1 ):
                            if (i >= len(selected_cols_pre_process)):
                                                    break
                            st.session_state.clean_df.dropna(subset=selected_cols_pre_process[i], inplace=True)
                            st.session_state.clean_df.reset_index(drop=True, inplace=True)

                            preprocessor = TextPreprocessor()
                            # def preprocess_text_parallel(text):
                            #     return preprocessor.preprocess_text(text)
                            # preprocessed_reviews = st.session_state.clean_df[selected_cols_pre_process[i]].apply(preprocess_text_parallel)
                            if 'perform_pos_tagging' in text_pre_process:
                                def preprocess_review_4(text):
                                    return preprocessor.preprocess_text_4(text ,selected_entities, output_format)

                                # Preprocess the reviews sequentially
                                preprocessed_reviews = [preprocess_review_4(text) for text in st.session_state.clean_df[selected_cols_pre_process[i]]]

                                # Unpack the preprocessed data
                                text_list,pos_tagging_list ,bag_of_words_list, ner_entities_list = zip(*preprocessed_reviews)
                            # # Add the preprocessed reviews as a new column in the DataFrame
                                pre_process_df=pd.DataFrame(st.session_state.clean_df)
                                if "pre_process_df" not in st.session_state:
                                    st.session_state.pre_process_df= pre_process_df   
                                #st.session_state.pre_process_df['Pre Process Text for '+ selected_cols_pre_process[i]] = preprocessed_reviews.astype(str)  
                                st.session_state.pre_process_df['PREPROCESSED_TEXT'] = pd.Series(text_list)
                                st.session_state.pre_process_df['POS_TAG'] = pd.Series(pos_tagging_list)
                                convert_dict = {'POS_TAG': str}
                                st.session_state.pre_process_df=st.session_state.pre_process_df.astype(convert_dict)
                                st.session_state.pre_process_df['BAG_OF_WORDS'] = pd.Series(bag_of_words_list)
                                st.session_state.pre_process_df['NER_ENTITIES'] = pd.Series(ner_entities_list)
                                    
                            else:
                                def preprocess_review(text):
                                    return preprocessor.preprocess_text(text, selected_entities, output_format)

                                # Preprocess the reviews sequentially
                                preprocessed_reviews = [preprocess_review(text) for text in st.session_state.clean_df[selected_cols_pre_process[i]]]

                                # Unpack the preprocessed data
                                text_list, bag_of_words_list, ner_entities_list = zip(*preprocessed_reviews)
                            # # Add the preprocessed reviews as a new column in the DataFrame
                                pre_process_df=pd.DataFrame(st.session_state.clean_df)
                                if "pre_process_df" not in st.session_state:
                                    st.session_state.pre_process_df= pre_process_df   
                                #st.session_state.pre_process_df['Pre Process Text for '+ selected_cols_pre_process[i]] = preprocessed_reviews.astype(str)  
                                st.session_state.pre_process_df['PREPROCESSED_TEXT'] = pd.Series(text_list)
                                st.session_state.pre_process_df['BAG_OF_WORDS'] = pd.Series(bag_of_words_list)
                                st.session_state.pre_process_df['NER_ENTITIES'] = pd.Series(ner_entities_list)
                            # Explode the NER entities then Separate the label and entity into separate columns and then Drop the temporary 'NER_ENTITIES' column
                            st.session_state.pre_process_df['NER_ENTITIES'].replace("NaN","PERSON,0",inplace=True)
                            duplicate_cols = st.session_state.pre_process_df.columns[st.session_state.pre_process_df.columns.duplicated()]
                            st.session_state.pre_process_df.drop(columns=duplicate_cols,inplace=True)
                            st.session_state.pre_process_df = st.session_state.pre_process_df.explode('NER_ENTITIES')
                            #st.session_state.pre_process_df[['LABEL', 'ENTITY']] = pd.DataFrame(st.session_state.pre_process_df['NER_ENTITIES'].tolist(), index=st.session_state.pre_process_df.index)
                            st.session_state.pre_process_df[['LABEL', 'ENTITY']] =st.session_state.pre_process_df['NER_ENTITIES'].apply(lambda x: pd.Series(str(x).split(",")))
                            st.session_state.pre_process_df['LABEL'].replace("nan"," ",inplace=True)
                            st.session_state.pre_process_df['LABEL'].replace('\W', '', regex=True,inplace=True)
                            st.session_state.pre_process_df['ENTITY'].replace('\W', '', regex=True,inplace=True)
                            st.session_state.pre_process_df.drop(columns='NER_ENTITIES', inplace=True)
                            if 'dictionary' in output_format:
                                # bag_of_words_df = pd.DataFrame.from_dict(st.session_state.pre_process_df['BAG_OF_WORDS'].to_list()).fillna(0)
                                # st.write(bag_of_words_df.columns)
                                # st.session_state.pre_process_df=st.session_state.pre_process_df.drop(columns=[list(bag_of_words_df.columns)],axis=1,inplace=True)
                                # st.write(st.session_state.pre_process_df)
                                st.session_state.pre_process_df['BAG_OF_WORDS'] = pd.Series(bag_of_words_list)
                                convert_dict = {'BAG_OF_WORDS': str}
                                st.session_state.pre_process_df=st.session_state.pre_process_df.astype(convert_dict)
                            if 'sparse_matrix' in output_format:
                                bag_of_words_df = pd.DataFrame.from_dict(st.session_state.pre_process_df['BAG_OF_WORDS'].to_list()).fillna(0)
                                st.session_state.pre_process_df.reset_index(drop=True, inplace=True)
                                st.session_state.pre_process_df = pd.concat([st.session_state.pre_process_df, bag_of_words_df], axis=1)
                                st.session_state.pre_process_df.drop(columns=['BAG_OF_WORDS'], inplace=True)
                            if "embeddings"  in text_pre_process:
                                embedding_type  = st.selectbox(label="Select Embedding Type",options=['sentence_embedding','word_embedding'])   
                                if "word_embedding" in embedding_type:
                                    embedding_technique  = st.selectbox(label="Select Embedding Technique for Word Embedding",options=['glove','fasttext','word2vec'])  
                                    st.session_state.pre_process_df['embeddings'] = get_word_embeddings(st.session_state.clean_df[selected_cols_pre_process[i]], embedding_technique)
                                if "sentence_embedding" in embedding_type:
                                    #embedding_technique  = st.selectbox(label="Select Embedding Technique for Sentence Embedding",options=['fasttext','word2vec'])  
                                    embedding_technique  = st.selectbox(label="Select Embedding Technique for Sentence Embedding",options=['MiniLM','DistilRoBERTa','RoBERTa','Ada'])  
                                    if embedding_technique == 'Ada':
                                        def get_embeddings_batch(texts_to_embed):
                                            # Embed the lines of text as a batch
                                            response = openai.Embedding.create(
                                                model="text-embedding-ada-002",
                                                input=texts_to_embed
                                            )
                                            # Extract the AI output embeddings as a list of lists of floats
                                            embeddings_batch = response["data"]
                                            embeddings = [item["embedding"] for item in embeddings_batch]
                                            return embeddings
                                        batch_size = 200
                                        # Split the texts into batches
                                        text_batches = [st.session_state.clean_df[selected_cols_pre_process[0]].astype(str)[i:i + batch_size].tolist() for i in range(0, len(st.session_state.pre_process_df), batch_size)]

                                        # List to store all embeddings
                                        all_embeddings = []

                                        # Iterate over batches and get embeddings
                                        for batch_texts in tqdm(text_batches):
                                            embeddings_batch = get_embeddings_batch(batch_texts)
                                            all_embeddings.extend(embeddings_batch)
                                        st.session_state.pre_process_df['embeddings']=all_embeddings
                                    else:
                                        st.session_state.pre_process_df = get_sentence_embeddings(st.session_state.clean_df[selected_cols_pre_process[i]], embedding_technique)
                            i=i+1 

                if 'create_bag_of_words' in text_pre_process and 'perform_ner' not in text_pre_process:
                    all_op_format=['sparse_matrix','dictionary']
                    output_format = st.multiselect("Choose the output format (sparse_matrix/dictionary) for bag of words",options=all_op_format)  
                    i = 0
                    while (i < len(selected_cols_pre_process) and len(text_pre_process)>= 1 and len(output_format)>=1):
                            if (i >= len(selected_cols_pre_process)):
                                                    break
                            st.session_state.clean_df.dropna(subset=selected_cols_pre_process[i], inplace=True)
                            st.session_state.clean_df.reset_index(drop=True, inplace=True)

                            preprocessor = TextPreprocessor()
                            # def preprocess_text_parallel(text):
                            #     return preprocessor.preprocess_text(text)
                            # preprocessed_reviews = st.session_state.clean_df[selected_cols_pre_process[i]].apply(preprocess_text_parallel)
                            if 'perform_pos_tagging' in text_pre_process:
                                def preprocess_review_5(text):
                                    return preprocessor.preprocess_text_5(text , output_format)

                                # Preprocess the reviews sequentially
                                preprocessed_reviews = [preprocess_review_5(text) for text in st.session_state.clean_df[selected_cols_pre_process[i]]]

                                # Unpack the preprocessed data
                                text_list,pos_tagging_list ,bag_of_words_list = zip(*preprocessed_reviews)
                            # # Add the preprocessed reviews as a new column in the DataFrame
                                pre_process_df=pd.DataFrame(st.session_state.clean_df)
                                if "pre_process_df" not in st.session_state:
                                    st.session_state.pre_process_df= pre_process_df   
                                #st.session_state.pre_process_df['Pre Process Text for '+ selected_cols_pre_process[i]] = preprocessed_reviews.astype(str)  
                                st.session_state.pre_process_df['PREPROCESSED_TEXT'] = pd.Series(text_list)
                                st.session_state.pre_process_df['POS_TAG'] = pd.Series(pos_tagging_list)
                                convert_dict = {'POS_TAG': str}
                                st.session_state.pre_process_df=st.session_state.pre_process_df.astype(convert_dict)
                                st.session_state.pre_process_df['BAG_OF_WORDS'] = pd.Series(bag_of_words_list)
                            else:
                                def preprocess_review_1(text):
                                    return preprocessor.preprocess_text_1(text, output_format)

                            # Preprocess the reviews sequentially
                                preprocessed_reviews = [preprocess_review_1(text) for text in st.session_state.clean_df[selected_cols_pre_process[i]]]

                                    # Unpack the preprocessed data
                                text_list, bag_of_words_list = zip(*preprocessed_reviews)
                                # # Add the preprocessed reviews as a new column in the DataFrame
                                pre_process_df=pd.DataFrame(st.session_state.clean_df)
                                if "pre_process_df" not in st.session_state:
                                        st.session_state.pre_process_df= pre_process_df
                                    #st.session_state.pre_process_df['Pre Process Text for '+ selected_cols_pre_process[i]] = preprocessed_reviews.astype(str)  
                                st.session_state.pre_process_df['PREPROCESSED_TEXT'] = pd.Series(text_list)
                                st.session_state.pre_process_df['BAG_OF_WORDS'] = pd.Series(bag_of_words_list)

                            if 'dictionary' in output_format:
                                # bag_of_words_df = pd.DataFrame.from_dict(st.session_state.pre_process_df['BAG_OF_WORDS'].to_list()).fillna(0)
                                # st.write(bag_of_words_df.columns)
                                # st.session_state.pre_process_df=st.session_state.pre_process_df.drop(columns=[list(bag_of_words_df.columns)],axis=1,inplace=True)
                                # st.write(st.session_state.pre_process_df)
                                st.session_state.pre_process_df['BAG_OF_WORDS'] = pd.Series(bag_of_words_list)
                                convert_dict = {'BAG_OF_WORDS': str}
                                st.session_state.pre_process_df=st.session_state.pre_process_df.astype(convert_dict)
                                # st.session_state.pre_process_df['BAG_OF_WORDS'].replace('\W', '', regex=True,inplace=True)
                            if 'sparse_matrix' in output_format:
                                bag_of_words_df = pd.DataFrame.from_dict(st.session_state.pre_process_df['BAG_OF_WORDS'].to_list()).fillna(0)
                                st.session_state.pre_process_df.reset_index(drop=True, inplace=True)
                                st.write(bag_of_words_df)
                                st.session_state.pre_process_df = pd.concat([st.session_state.pre_process_df, bag_of_words_df], axis=1)
                                st.session_state.pre_process_df.drop(columns=['BAG_OF_WORDS'], inplace=True)
                            if "embeddings"  in text_pre_process:
                                embedding_type  = st.selectbox(label="Select Embedding Type",options=['sentence_embedding','word_embedding'])   
                                if "word_embedding" in embedding_type:
                                    embedding_technique  = st.selectbox(label="Select Embedding Technique for Word Embedding",options=['glove','fasttext','word2vec'])  
                                    st.session_state.pre_process_df['embeddings'] = get_word_embeddings(st.session_state.clean_df[selected_cols_pre_process[i]], embedding_technique)
                                if "sentence_embedding" in embedding_type:
                                    #embedding_technique  = st.selectbox(label="Select Embedding Technique for Sentence Embedding",options=['fasttext','word2vec'])  
                                    embedding_technique  = st.selectbox(label="Select Embedding Technique for Sentence Embedding",options=['MiniLM','DistilRoBERTa','RoBERTa','Ada'])  
                                    if embedding_technique == 'Ada':
                                        def get_embeddings_batch(texts_to_embed):
                                            # Embed the lines of text as a batch
                                            response = openai.Embedding.create(
                                                model="text-embedding-ada-002",
                                                input=texts_to_embed
                                            )
                                            # Extract the AI output embeddings as a list of lists of floats
                                            embeddings_batch = response["data"]
                                            embeddings = [item["embedding"] for item in embeddings_batch]
                                            return embeddings
                                        batch_size = 200
                                        # Split the texts into batches
                                        text_batches = [st.session_state.clean_df[selected_cols_pre_process[0]].astype(str)[i:i + batch_size].tolist() for i in range(0, len(st.session_state.pre_process_df), batch_size)]

                                        # List to store all embeddings
                                        all_embeddings = []

                                        # Iterate over batches and get embeddings
                                        for batch_texts in tqdm(text_batches):
                                            embeddings_batch = get_embeddings_batch(batch_texts)
                                            all_embeddings.extend(embeddings_batch)
                                        st.session_state.pre_process_df['embeddings']=all_embeddings
                                    else:
                                        st.session_state.pre_process_df = get_sentence_embeddings(st.session_state.clean_df[selected_cols_pre_process[i]], embedding_technique)    
                            i=i+1 

                if 'perform_ner' in text_pre_process and 'create_bag_of_words' not in text_pre_process:
                    all_entities = ('person','organization','location','basic')
                    selected_entities=st.multiselect("Select Entities for NER",options=all_entities)
                    i = 0
                    while (i < len(selected_cols_pre_process) and len(text_pre_process)>= 1 and len(selected_entities)>=1 ):
                            if (i >= len(selected_cols_pre_process)):
                                                    break
                            st.session_state.clean_df.dropna(subset=selected_cols_pre_process[i], inplace=True)
                            st.session_state.clean_df.reset_index(drop=True, inplace=True)

                            preprocessor = TextPreprocessor()
                            # def preprocess_text_parallel(text):
                            #     return preprocessor.preprocess_text(text)
                            # preprocessed_reviews = st.session_state.clean_df[selected_cols_pre_process[i]].apply(preprocess_text_parallel)
                            if 'perform_pos_tagging' in text_pre_process:
                                def preprocess_review_6(text):
                                    return preprocessor.preprocess_text_6(text, selected_entities)

                                # Preprocess the reviews sequentially
                                preprocessed_reviews = [preprocess_review_6(text) for text in st.session_state.clean_df[selected_cols_pre_process[i]]]

                                # Unpack the preprocessed data
                                text_list,pos_tagging_list ,ner_entities_list = zip(*preprocessed_reviews)
                            # # Add the preprocessed reviews as a new column in the DataFrame
                                pre_process_df=pd.DataFrame(st.session_state.clean_df)
                                if "pre_process_df" not in st.session_state:
                                    st.session_state.pre_process_df= pre_process_df
                                #st.session_state.pre_process_df['Pre Process Text for '+ selected_cols_pre_process[i]] = preprocessed_reviews.astype(str)  
                                st.session_state.pre_process_df['PREPROCESSED_TEXT'] = pd.Series(text_list)
                                st.session_state.pre_process_df['POS_TAG'] = pd.Series(pos_tagging_list)
                                convert_dict = {'POS_TAG': str}
                                st.session_state.pre_process_df=st.session_state.pre_process_df.astype(convert_dict)
                                st.session_state.pre_process_df['NER_ENTITIES'] = pd.Series(ner_entities_list)
                            
                            else:
                                def preprocess_review_2(text):
                                    return preprocessor.preprocess_text_2(text, selected_entities)

                            # Preprocess the reviews sequentially
                                preprocessed_reviews = [preprocess_review_2(text) for text in st.session_state.clean_df[selected_cols_pre_process[i]]]

                                # Unpack the preprocessed data
                                text_list, ner_entities_list = zip(*preprocessed_reviews)
                            # # Add the preprocessed reviews as a new column in the DataFrame
                                pre_process_df=pd.DataFrame(st.session_state.clean_df)
                                if "pre_process_df" not in st.session_state:
                                    st.session_state.pre_process_df= pre_process_df
                                #st.session_state.pre_process_df['Pre Process Text for '+ selected_cols_pre_process[i]] = preprocessed_reviews.astype(str)  
                                st.session_state.pre_process_df['PREPROCESSED_TEXT'] = pd.Series(text_list)
                                st.session_state.pre_process_df['NER_ENTITIES'] = pd.Series(ner_entities_list)
                            # Explode the NER entities then Separate the label and entity into separate columns and then Drop the temporary 'NER_ENTITIES' column
                            st.session_state.pre_process_df['NER_ENTITIES'].replace("NaN","PERSON,0",inplace=True)
                            st.session_state.pre_process_df = st.session_state.pre_process_df.explode('NER_ENTITIES')
                            #st.session_state.pre_process_df[['LABEL', 'ENTITY']] = pd.DataFrame(st.session_state.pre_process_df['NER_ENTITIES'].tolist(), index=st.session_state.pre_process_df.index)
                            st.session_state.pre_process_df[['LABEL', 'ENTITY']] =st.session_state.pre_process_df['NER_ENTITIES'].apply(lambda x: pd.Series(str(x).split(",")))
                            st.session_state.pre_process_df['LABEL'].replace("nan"," ",inplace=True)
                            st.session_state.pre_process_df['LABEL'].replace('\W', '', regex=True,inplace=True)
                            st.session_state.pre_process_df['ENTITY'].replace('\W', '', regex=True,inplace=True)
                            st.session_state.pre_process_df.drop(columns='NER_ENTITIES', inplace=True)
                            if "embeddings"  in text_pre_process:
                                embedding_type  = st.selectbox(label="Select Embedding Type",options=['sentence_embedding','word_embedding'])   
                                if "word_embedding" in embedding_type:
                                    embedding_technique  = st.selectbox(label="Select Embedding Technique for Word Embedding",options=['glove','fasttext','word2vec'])  
                                    st.session_state.pre_process_df['embeddings'] = get_word_embeddings(st.session_state.clean_df[selected_cols_pre_process[i]], embedding_technique)
                                if "sentence_embedding" in embedding_type:
                                    #embedding_technique  = st.selectbox(label="Select Embedding Technique for Sentence Embedding",options=['fasttext','word2vec'])  
                                    embedding_technique  = st.selectbox(label="Select Embedding Technique for Sentence Embedding",options=['MiniLM','DistilRoBERTa','RoBERTa','Ada'])  
                                    if embedding_technique == 'Ada':
                                        def get_embeddings_batch(texts_to_embed):
                                            # Embed the lines of text as a batch
                                            response = openai.Embedding.create(
                                                model="text-embedding-ada-002",
                                                input=texts_to_embed
                                            )
                                            # Extract the AI output embeddings as a list of lists of floats
                                            embeddings_batch = response["data"]
                                            embeddings = [item["embedding"] for item in embeddings_batch]
                                            return embeddings
                                        batch_size = 200
                                        # Split the texts into batches
                                        text_batches = [st.session_state.clean_df[selected_cols_pre_process[0]].astype(str)[i:i + batch_size].tolist() for i in range(0, len(st.session_state.pre_process_df), batch_size)]

                                        # List to store all embeddings
                                        all_embeddings = []

                                        # Iterate over batches and get embeddings
                                        for batch_texts in tqdm(text_batches):
                                            embeddings_batch = get_embeddings_batch(batch_texts)
                                            all_embeddings.extend(embeddings_batch)
                                        st.session_state.pre_process_df['embeddings']=all_embeddings
                                    else:
                                        st.session_state.pre_process_df = get_sentence_embeddings(st.session_state.clean_df[selected_cols_pre_process[i]], embedding_technique)
                            i=i+1   

                if 'perform_ner' not in text_pre_process and 'create_bag_of_words' not in text_pre_process:
                    i = 0
                    while (i < len(selected_cols_pre_process) and len(text_pre_process)>= 1 ):
                            if (i >= len(selected_cols_pre_process)):
                                                    break
                            st.session_state.clean_df.dropna(subset=selected_cols_pre_process[i], inplace=True)
                            st.session_state.clean_df.reset_index(drop=True, inplace=True)

                            preprocessor = TextPreprocessor()
                            # def preprocess_text_parallel(text):
                            #     return preprocessor.preprocess_text(text)
                            # preprocessed_reviews = st.session_state.clean_df[selected_cols_pre_process[i]].apply(preprocess_text_parallel)
                            if 'perform_pos_tagging' in text_pre_process:
                                def preprocess_review_7(text):
                                    return preprocessor.preprocess_text_7(text)

                                # Preprocess the reviews sequentially
                                preprocessed_reviews = [preprocess_review_7(text) for text in st.session_state.clean_df[selected_cols_pre_process[i]]]

                                # Unpack the preprocessed data
                                text_list,pos_tagging_list = zip(*preprocessed_reviews)
                            # # Add the preprocessed reviews as a new column in the DataFrame
                                pre_process_df=pd.DataFrame(st.session_state.clean_df)
                                if "pre_process_df" not in st.session_state:
                                    st.session_state.pre_process_df= pre_process_df
                                #st.session_state.pre_process_df['Pre Process Text for '+ selected_cols_pre_process[i]] = preprocessed_reviews.astype(str)  
                                st.session_state.pre_process_df['PREPROCESSED_TEXT'] = pd.Series(text_list)
                                st.session_state.pre_process_df['POS_TAG'] = pd.Series(pos_tagging_list)
                                convert_dict = {'POS_TAG': str}
                                st.session_state.pre_process_df=st.session_state.pre_process_df.astype(convert_dict)
                            else:    
                                def preprocess_review_3(text):
                                    return preprocessor.preprocess_text_3(text)

                                # Preprocess the reviews sequentially
                                preprocessed_reviews = [preprocess_review_3(text) for text in st.session_state.clean_df[selected_cols_pre_process[i]].astype(str)]

                                # Unpack the preprocessed data
                                text_list = preprocessed_reviews
                            # # Add the preprocessed reviews as a new column in the DataFrame
                                pre_process_df=pd.DataFrame(st.session_state.clean_df)
                                if "pre_process_df" not in st.session_state:
                                    st.session_state.pre_process_df= pre_process_df
                                #st.session_state.pre_process_df['Pre Process Text for '+ selected_cols_pre_process[i]] = preprocessed_reviews.astype(str)  
                                st.session_state.pre_process_df['PREPROCESSED_TEXT'] = text_list
                            if "embeddings"  in text_pre_process:
                                embedding_type  = st.selectbox(label="Select Embedding Type",options=['sentence_embedding','word_embedding'])   
                                if "word_embedding" in embedding_type:
                                    embedding_technique  = st.selectbox(label="Select Embedding Technique for Word Embedding",options=['glove','fasttext','word2vec'])  
                                    st.session_state.pre_process_df['embeddings'] = get_word_embeddings(st.session_state.clean_df[selected_cols_pre_process[i]], embedding_technique)
                                if "sentence_embedding" in embedding_type:
                                    #embedding_technique  = st.selectbox(label="Select Embedding Technique for Sentence Embedding",options=['fasttext','word2vec'])  
                                    embedding_technique  = st.selectbox(label="Select Embedding Technique for Sentence Embedding",options=['MiniLM','DistilRoBERTa','RoBERTa','Ada'])  
                                    if embedding_technique == 'Ada':
                                        def get_embeddings_batch(texts_to_embed):
                                            # Embed the lines of text as a batch
                                            response = openai.Embedding.create(
                                                model="text-embedding-ada-002",
                                                input=texts_to_embed
                                            )
                                            # Extract the AI output embeddings as a list of lists of floats
                                            embeddings_batch = response["data"]
                                            embeddings = [item["embedding"] for item in embeddings_batch]
                                            return embeddings
                                        batch_size = 200
                                        # Split the texts into batches
                                        text_batches = [st.session_state.clean_df[selected_cols_pre_process[0]].astype(str)[i:i + batch_size].tolist() for i in range(0, len(st.session_state.pre_process_df), batch_size)]

                                        # List to store all embeddings
                                        all_embeddings = []

                                        # Iterate over batches and get embeddings
                                        for batch_texts in tqdm(text_batches):
                                            embeddings_batch = get_embeddings_batch(batch_texts)
                                            all_embeddings.extend(embeddings_batch)
                                        st.session_state.pre_process_df['embeddings']=all_embeddings
                                    else:
                                        st.session_state.pre_process_df = get_sentence_embeddings(st.session_state.clean_df[selected_cols_pre_process[i]], embedding_technique)    
                            i=i+1                                           
                if "pre_process_df" in st.session_state:          
                    gb = GridOptionsBuilder.from_dataframe(st.session_state.pre_process_df)

                    gb.configure_pagination()
                    gb.configure_side_bar()
                    gb.configure_default_column(groupable=True, value=True, enableRowGroup=True, aggFunc="sum", editable=True)
                    gridOptions = gb.build()
                    AgGrid(st.session_state.pre_process_df, gridOptions=gridOptions, enable_enterprise_modules=True)
                    if "df_viz" not in st.session_state:
                        st.session_state.df_viz=st.session_state.pre_process_df

                    if "df_viz" in st.session_state:
                        st.session_state.df_viz=st.session_state.pre_process_df    
                    csv = convert_df(st.session_state.pre_process_df)
                    st.download_button(
                            label="Download data as CSV",
                            data=csv,
                            file_name='pre_process_data.csv',
                            mime='text/csv',
                        )         
            else :
                st.header('Load Your data to get started')

elif options == 'Sentiment Analysis':
    if uploaded_file is not None or st.session_state['button'] == True:
        if "sentiment_df" not in st.session_state and "pre_process_df" in st.session_state:
            st.session_state.sentiment_df=st.session_state.pre_process_df  

        if "sentiment_df" not in st.session_state and "pre_process_df" not in st.session_state and "clean_df" in st.session_state:
            st.session_state.sentiment_df=st.session_state.clean_df 

        if "sentiment_df" not in st.session_state and "pre_process_df" not in st.session_state and "clean_df" not in st.session_state and "sampled_df" in st.session_state :
            st.session_state.sentiment_df=st.session_state.sampled_df  

        if "sentiment_df" not in st.session_state and "pre_process_df" not in st.session_state and "clean_df" not in st.session_state and "sampled_df" not in st.session_state:
            st.session_state.sentiment_df=st.session_state.df 

        import sentiment_analysis_vader
        import sentiment_analysis_text_blob
        import sentiment_analysis_roberta
        import sentiment_analysis_flair
        import sentiment_analysis_sentiment_score_final
        import sentiment_analysis_key_phrase_extraction_using_key_bert
        import sentiment_analysis_word_cloud

        # Read in data
        df = pd.DataFrame(st.session_state.sentiment_df)
        # df = df.fillna('')

        col_name_for_sentiment = st.multiselect(label="Select Column for Sentiment Analysis",options=df.columns,key="Column For Sentiment Ananlysis")
        if st.button("Run Sentiment Analysis"):
            col_name=col_name_for_sentiment[0]
            if "col_name_sentiment" not in st.session_state:
                st.session_state.col_name_sentiment = col_name
            df.dropna(subset=col_name_for_sentiment[0], inplace=True)
            df.reset_index(drop=True, inplace=True)    
            sentiment_analysis_vader.class_vader_sentiment_analysis.vader_sentiment_analysis(df, col_name)
            sentiment_analysis_text_blob.text_blob_sentiment_analysis.text_blob_sentiment_analysis(df, col_name)
            sentiment_analysis_roberta.class_roberta_sentiment_analysis.roberta_sentiment_analysis(df, col_name)
            sentiment_analysis_flair.class_flair_sentiment_analysis.flair_sentiment_analysis(df, col_name)
            sentiment_df = sentiment_analysis_sentiment_score_final.class_sentiment_score.finalize_sentiment_score(df)
            df['sentiment'] = sentiment_df['sentiment_final_score'].apply(sentiment_analysis_sentiment_score_final.class_sentiment_score.getAnalysis)
            df_to_display = df.drop(['flair_sentiment','flair_score','vader_score','vader_sentiment','textBlob_score','TextBlob_sentiment','roberta_sentiment','roberta_score','sentiment_mean','sentiment_final_score'], axis = 1)
            st.write(df_to_display)
            if "sentiment_df_2" not in st.session_state and "sentiment_df" in st.session_state:
                st.session_state.sentiment_df_2=df 

            # positive_plot = sentiment_analysis_word_cloud.class_sentiment_word_cloud.AssignColor.out_put_word_cloud(positive_phrase_df, positive_phrase_df['key_phrase'].values.tolist())
            # # positive_plot.show()
            # st.image(wc.to_image(), use_container_width=True, channels='RGBA')

        else:
            st.error("Please Select A Column For Sentiment Analysis.")

        if "sentiment_df_2" in st.session_state and "sentiment_df" in st.session_state:
            csv = convert_df(st.session_state.sentiment_df_2)
            st.download_button(
                                    label="Download data as CSV",
                                    data=csv,
                                    file_name='Sentiment_Analysis.csv',
                                    mime='text/csv',
                                )  
    else:
        st.header('Load Your data to get started')   
         
elif options == 'Key Phrase Extraction': 
            import sentiment_analysis_key_phrase_extraction_using_key_bert
            import sentiment_analysis_word_cloud
            if "sentiment_df_2" in st.session_state:
                df = pd.DataFrame(st.session_state.sentiment_df_2)
            else:
                st.write("Run Sentiment Analysis First")

            col_name = st.session_state.col_name_sentiment
            key_phrase_df = sentiment_analysis_key_phrase_extraction_using_key_bert.class_key_phrase_extraction.key_bert(df.reset_index(), col_name)
            key_phrase_df['sentiment_final_score']=df['sentiment']
            exploded_df = sentiment_analysis_key_phrase_extraction_using_key_bert.class_key_phrase_extraction.explode_keybert_df(key_phrase_df)
            exploded_reset_index_df = sentiment_analysis_key_phrase_extraction_using_key_bert.class_key_phrase_extraction.resetting_and_adding_index_col_to_df(exploded_df)

            phrase_df = sentiment_analysis_key_phrase_extraction_using_key_bert.class_key_phrase_extraction.separating_keyphrase_rows_exploded_df(exploded_reset_index_df)
            similarity_score = sentiment_analysis_key_phrase_extraction_using_key_bert.class_key_phrase_extraction.separating_SimilarityScore_rows_exploded_df(exploded_reset_index_df)

            positive_phrase_df = sentiment_analysis_key_phrase_extraction_using_key_bert.class_key_phrase_extraction.finalize_key_phrase(exploded_df)
            user_output_df = sentiment_analysis_key_phrase_extraction_using_key_bert.class_key_phrase_extraction.output_keyPhrase_similarityScore_df_to_user(positive_phrase_df,key_phrase_df) 
            #user_output_df = sentiment_analysis_key_phrase_extraction_using_key_bert.class_key_phrase_extraction.output_keyPhrase_similarityScore_df_to_user(exploded_df) 
            st.write(positive_phrase_df[['key_phrase','similarity_score','sentiment_final_score']]) 
            #positive_plot = sentiment_analysis_word_cloud.class_sentiment_word_cloud.AssignColor.out_put_word_cloud(positive_phrase_df, positive_phrase_df['key_phrase'].values.tolist())
            #positive_plot.show()
            #st.image(wc.to_image(), use_container_width=True, channels='RGBA') 
            import streamlit as st
            import pandas as pd
            from nltk.corpus import stopwords
            from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
            from wordcloud import WordCloud, get_single_color_func
            import matplotlib.pyplot as plt
            import sentiment_analysis_word_cloud
            # Call your SentimentWordCloud class to generate the word cloud
            if st.button("Generate Word Cloud"):
                # Replace this with your data
                positive_phrase_df_1 = pd.DataFrame(positive_phrase_df)
                review_list = positive_phrase_df_1['key_phrase'].values.tolist()
               # AssignColor.generate_word_cloud(positive_phrase_df_1, review_list)
                sentiment_analysis_word_cloud.class_sentiment_word_cloud.AssignColor.out_put_word_cloud(positive_phrase_df_1, review_list)
  

elif options=='Topic Modelling':
        if "model_df" not in st.session_state and "pre_process_df" in st.session_state :
                st.session_state.model_df=st.session_state.pre_process_df
                if "embeddings" in st.session_state.model_df:
                    import importlib
                    import clustering
                    import summarizer  # Import the summarizer module

                    importlib.reload(clustering)
                    importlib.reload(summarizer)  # Ensure the latest version is used

                    from clustering import ClusterAnalyzer
                    from cluster_info import ClusterTrendVisualizer
                    #embeddings = np.vstack(st.session_state.model_df.embeddings.values)
                    df2=pd.DataFrame(st.session_state.model_df)
                    selected_cols_topic_modelling=df2["PREPROCESSED_TEXT"]
                    select_date_col=st.selectbox(label="Select Date ",options=df2.columns,key='Date Select')
                    analyzer = ClusterAnalyzer(api_key=openai.api_key)
                    result_df = analyzer.analyze_and_visualize(df2,selected_cols_topic_modelling)
                    # Visualize the overall cluster distribution and trend for each cluster
                    visualizer = ClusterTrendVisualizer(result_df,selected_cols_topic_modelling)
                    visualizer.plot_overall_cluster_distribution()
                    visualizer.plot_trend_for_each_cluster(selected_cols_topic_modelling)
                    st.session_state.model_df = result_df                     
                    # embeddings = np.vstack(st.session_state.model_df.embeddings.values)
                    # range_n_clusters = list(range(2,10))
                    # best_num_clusters = 2
                    # best_silhouette = -1

                    # for n_clusters in range_n_clusters:
                    #     clusterer = KMeans(n_clusters=n_clusters, random_state=42)
                    #     cluster_labels = clusterer.fit_predict(embeddings)
                    #     silhouette_avg = silhouette_score(embeddings, cluster_labels)
                    #     #print(f"Silhouette score for {n_clusters} clusters: {silhouette_avg:.4f}")

                    #     if silhouette_avg > best_silhouette:
                    #         best_silhouette = silhouette_avg
                    #         best_num_clusters = n_clusters

                    # # Using the best number of clusters to cluster data
                    # kmeans = KMeans(n_clusters=best_num_clusters, random_state=42)
                    # st.session_state.model_df['cluster'] = kmeans.fit_predict(embeddings)

                    # # ... existing code

                    # # Initialize an empty dictionary to store cluster names
                    # cluster_name_dict = {}

                    # # Existing code for generating cluster names
                    # prompt = 'Based on the following complaints, please suggest a name for this cluster that encapsulates the primary issues or problems mentioned.'
                    # for j in range(best_num_clusters):
                    #     # Calculate the number of reviews that belong to the current cluster
                    #     num_reviews_in_cluster = len(st.session_state.model_df[st.session_state.model_df['cluster'] == j])
                        
                    #     # Calculate 50% of the number of reviews in the cluster, rounding up
                    #     rev_per_cluster = int(np.ceil(num_reviews_in_cluster * 0.5))
                        
                    #     # Initialize variable for token count
                    #     token_count = 0
                    #     # Initialize sample percentage
                    #     sample_percentage = 0.5

                    #     while True:
                    #         # Get a sample of reviews for the current cluster
                    #         sample_reviews = st.session_state.model_df[st.session_state.model_df['cluster'] == j].PREPROCESSED_TEXT.sample(min(rev_per_cluster, num_reviews_in_cluster), random_state=41).values
                    #         # Calculate the number of tokens in the sample
                    #         token_count = sum(len(review.split()) for review in sample_reviews)
                            
                    #         # Check if the token count is under the 16,000-token limit
                    #         if token_count <= 10000:
                    #             break
                    #         else:
                    #             # Reduce the sample percentage by 10% each time
                    #             sample_percentage -= 0.05
                    #             # Re-calculate the number of reviews to sample based on the reduced percentage
                    #             rev_per_cluster = int(np.ceil(num_reviews_in_cluster * sample_percentage))

                    #     # Get the finalized sample of reviews for the current cluster
                    #     complaints = "\n".join(sample_reviews)
                        
                    #     cluster_name = openai.ChatCompletion.create(
                    #         model="gpt-3.5-turbo-16k",
                    #         #engine="Chat",
                    #         messages=[
                    #             {"role": "system", "content": prompt},
                    #             {"role": "user", "content": complaints}
                    #         ]
                    #     )['choices'][0]['message']['content']
                        
                    #     # Store the cluster name in the dictionary with cluster number as key
                    #     cleaned_cluster_name = cluster_name.replace("Product Problem Cluster: ", "").strip()
                    #     cluster_name_dict[j] = cleaned_cluster_name

                    #     # Map the cluster names back to the DataFrame
                    # st.session_state.model_df['cluster_name'] = st.session_state.model_df['cluster'].map(cluster_name_dict)
                    #     #st.write(st.session_state.model_df)
                    # gb = GridOptionsBuilder.from_dataframe(st.session_state.model_df)
                    # gb.configure_pagination()
                    # gb.configure_side_bar()
                    # gb.configure_default_column(groupable=True, value=True, enableRowGroup=True, aggFunc="sum", editable=True)
                    # gridOptions = gb.build()      
                    # AgGrid(st.session_state.model_df, gridOptions=gridOptions, enable_enterprise_modules=True)
                

                    # n_clusters = best_num_clusters

                    # kmeans = KMeans(n_clusters=n_clusters, init="k-means++", random_state=42)
                    # kmeans.fit(embeddings)
                    # labels = kmeans.labels_
                    # # st.session_state.model_df["Cluster"] = labels

                    # # Change the random state for t-SNE
                    # tsne = TSNE(n_components=3, perplexity=15, random_state=41, init="random", learning_rate=200)
                    # vis_dims3 = tsne.fit_transform(embeddings)

                    # x = [x for x, y, z in vis_dims3]
                    # y = [y for x, y, z in vis_dims3]
                    # z = [z for x, y, z in vis_dims3]

                    # # Create a figure
                    # fig = plt.figure(figsize=(25, 25))
                    # ax = fig.add_subplot(111, projection='3d')

                    # # Visualize clusters using Scatter plots
                    # for category, color in enumerate(["purple", "green", "red", "blue", "yellow", "orange", 'black', 'brown', 'grey', 'olive', 'cyan', 'pink']):
                    #     xs = np.array(x)[st.session_state.model_df.cluster == category]
                    #     ys = np.array(y)[st.session_state.model_df.cluster == category]
                    #     zs = np.array(z)[st.session_state.model_df.cluster == category]
                    #     ax.scatter(xs, ys, zs, color=color, alpha=0.3, label=cluster_name)

                    #     avg_x = xs.mean()
                    #     avg_y = ys.mean()
                    #     avg_z = zs.mean()

                    #     ax.scatter(avg_x, avg_y, avg_z, marker="x", color=color, s=100)
                    #     # Annotate with the cluster name
                    #     cluster_name = cluster_name_dict.get(category, f"Cluster {category}")  # Default to "Cluster {category}" if not found
                    #     ax.text(avg_x, avg_y, avg_z, cluster_name, fontsize=16)
                    # ax.legend(title="Clusters")
                    # ax.set_title("Clusters identified visualized in language 3D using t-SNE")

                    # # Display the plot using Streamlit
                    # st.pyplot(fig)
                    # size_and_cluster_number = pd.DataFrame(st.session_state.model_df.groupby("cluster_name").size().sort_values(ascending=False)).reset_index()
                    # size_and_cluster_number
                    # # print out 10 messages from n cluster
                    # for index, row in size_and_cluster_number.iterrows():
                    #     cluster_name = row['cluster_name']
                    #     cluster_size = row[0]
                    #     st.write(f"The name of the cluster is {cluster_name} and its size is {cluster_size} complaints.")
                    #     st.write(st.session_state.model_df[st.session_state.model_df.cluster_name == cluster_name].sample(10)["PREPROCESSED_TEXT"].values)
                    #     st.write("\n")
                             
        if "model_df" not in st.session_state and "pre_process_df" not in st.session_state and "clean_df" in st.session_state:
            st.session_state.model_df=st.session_state.clean_df

        if "model_df" not in st.session_state and "pre_process_df" not in st.session_state and "clean_df" not in st.session_state and "df" in st.session_state:  
            st.session_state.model_df=st.session_state.df         

        if  uploaded_file is None and st.session_state['button'] == True :
            df=get_table_from_sql()
            st.session_state.model_df=pd.DataFrame(df) 

        if "pre_process_df" not in st.session_state or "embeddings" not in st.session_state.pre_process_df:
            selected_cols_topic_modelling = st.multiselect(
                'Choose Column for topic_modelling:',
                st.session_state.model_df.select_dtypes(include='object').columns,
                key='Topic Modelling'
            )
            select_date_col = st.multiselect('Select Date', st.session_state.model_df.columns)

            if "selected_cols_topic_modelling" not in st.session_state:
                st.session_state.selected_cols_topic_modelling = selected_cols_topic_modelling

            if "selected_cols_topic_modelling" in st.session_state:
                st.session_state.selected_cols_topic_modelling = selected_cols_topic_modelling

            embedding_type = st.selectbox(
                label="Select Embedding Type",
                options=['sentence_embedding', 'word_embedding'],
                key='Embedding Type'
            )

            if "sentence_embedding" in embedding_type:
                embedding_technique = st.selectbox(
                    label="Select Embedding Technique for Sentence Embedding",
                    options=['MiniLM', 'DistilRoBERTa', 'RoBERTa', 'Ada'],
                    key='Embedding Technique'
                )
            else:
                embedding_technique = st.selectbox(
                    label="Select Embedding Technique for Word Embedding",
                    options=['glove', 'fasttext', 'word2vec'],
                    key='Embedding Technique'
                )

            if st.button("Run Topic Modelling", key="Run Button"):
                i = 0
                start = 1

                while i < len(selected_cols_topic_modelling):
                    if start == 1:
                        st.session_state.model_df.dropna(subset=[selected_cols_topic_modelling[i]], inplace=True)
                        st.session_state.model_df.reset_index(drop=True, inplace=True)

                        if "word_embedding" in embedding_type:
                            st.session_state.model_df['embeddings'] = get_word_embeddings(
                                st.session_state.model_df[selected_cols_topic_modelling[i]],
                                embedding_technique
                            )

                        if "sentence_embedding" in embedding_type:
                            if embedding_technique == 'Ada':
                                def get_embeddings_batch(texts_to_embed):
                                    response = openai.Embedding.create(
                                        model="text-embedding-ada-002",
                                        input=texts_to_embed
                                    )
                                    embeddings_batch = response["data"]
                                    embeddings = [item["embedding"] for item in embeddings_batch]
                                    return embeddings

                                batch_size = 200
                                text_batches = [
                                    st.session_state.model_df[selected_cols_topic_modelling[0]].astype(str)[i:i + batch_size].tolist()
                                    for i in range(0, len(st.session_state.model_df), batch_size)
                                ]

                                all_embeddings = []

                                for batch_texts in tqdm(text_batches):
                                    embeddings_batch = get_embeddings_batch(batch_texts)
                                    all_embeddings.extend(embeddings_batch)

                                st.session_state.model_df['embeddings'] = all_embeddings

                            else:
                                st.session_state.model_df = get_sentence_embeddings(
                                    st.session_state.model_df[selected_cols_topic_modelling[i]],
                                    embedding_technique
                                )

                        from clustering import ClusterAnalyzer
                        from cluster_info import ClusterTrendVisualizer

                        df2 = pd.DataFrame(st.session_state.model_df)
                        analyzer = ClusterAnalyzer(api_key=openai.api_key)
                        result_df = analyzer.analyze_and_visualize(df2, selected_cols_topic_modelling)

                        visualizer = ClusterTrendVisualizer(result_df, selected_cols_topic_modelling)
                        visualizer.plot_overall_cluster_distribution()
                        visualizer.plot_trend_for_each_cluster(selected_cols_topic_modelling, select_date_col)
                        st.session_state.model_df = result_df

                    i += 1
                    start += 1
                            #     embeddings = np.vstack(st.session_state.model_df.embeddings.values)
                #     range_n_clusters = list(range(2,10))
                #     best_num_clusters = 2
                #     best_silhouette = -1

                #     for n_clusters in range_n_clusters:
                #         clusterer = KMeans(n_clusters=n_clusters, random_state=42)
                #         cluster_labels = clusterer.fit_predict(embeddings)
                #         silhouette_avg = silhouette_score(embeddings, cluster_labels)
                #         #print(f"Silhouette score for {n_clusters} clusters: {silhouette_avg:.4f}")

                #         if silhouette_avg > best_silhouette:
                #             best_silhouette = silhouette_avg
                #             best_num_clusters = n_clusters

                #     # Using the best number of clusters to cluster data
                #     kmeans = KMeans(n_clusters=best_num_clusters, random_state=42)
                #     st.session_state.model_df['cluster'] = kmeans.fit_predict(embeddings)

                #     # ... existing code

                #     # Initialize an empty dictionary to store cluster names
                #     cluster_name_dict = {}

                #     # Existing code for generating cluster names
                #     prompt = 'Based on the following complaints, please suggest a name for this cluster that encapsulates the primary issues or problems mentioned.'
                #     for j in range(best_num_clusters):
                #         # Calculate the number of reviews that belong to the current cluster
                #         num_reviews_in_cluster = len(st.session_state.model_df[st.session_state.model_df['cluster'] == j])
                        
                #         # Calculate 50% of the number of reviews in the cluster, rounding up
                #         rev_per_cluster = int(np.ceil(num_reviews_in_cluster * 0.5))
                        
                #         # Initialize variable for token count
                #         token_count = 0
                #         # Initialize sample percentage
                #         sample_percentage = 0.5

                #         while True:
                #             # Get a sample of reviews for the current cluster
                #             sample_reviews = st.session_state.model_df[st.session_state.model_df['cluster'] == j][selected_cols_topic_modelling[i]].sample(min(rev_per_cluster, num_reviews_in_cluster), random_state=41).values
                #             # Calculate the number of tokens in the sample
                #             token_count = sum(len(review.split()) for review in sample_reviews)
                            
                #             # Check if the token count is under the 16,000-token limit
                #             if token_count <= 10000:
                #                 break
                #             else:
                #                 # Reduce the sample percentage by 10% each time
                #                 sample_percentage -= 0.05
                #                 # Re-calculate the number of reviews to sample based on the reduced percentage
                #                 rev_per_cluster = int(np.ceil(num_reviews_in_cluster * sample_percentage))

                #         # Get the finalized sample of reviews for the current cluster
                #         complaints = "\n".join(sample_reviews)
                        
                #         cluster_name = openai.ChatCompletion.create(
                #             model="gpt-3.5-turbo-16k",
                #             #engine="Chat",
                #             messages=[
                #                 {"role": "system", "content": prompt},
                #                 {"role": "user", "content": complaints}
                #             ]
                #         )['choices'][0]['message']['content']
                        
                #         # Store the cluster name in the dictionary with cluster number as key
                #         cleaned_cluster_name = cluster_name.replace("Product Problem Cluster: ", "").strip()
                #         cluster_name_dict[j] = cleaned_cluster_name

                #         # Map the cluster names back to the DataFrame
                #     st.session_state.model_df['cluster_name'] = st.session_state.model_df['cluster'].map(cluster_name_dict)
                #         #st.write(st.session_state.model_df)
                #     gb = GridOptionsBuilder.from_dataframe(st.session_state.model_df)
                #     gb.configure_pagination()
                #     gb.configure_side_bar()
                #     gb.configure_default_column(groupable=True, value=True, enableRowGroup=True, aggFunc="sum", editable=True)
                #     gridOptions = gb.build()      
                #     AgGrid(st.session_state.model_df, gridOptions=gridOptions, enable_enterprise_modules=True)
                

                #     n_clusters = best_num_clusters

                #     kmeans = KMeans(n_clusters=n_clusters, init="k-means++", random_state=42)
                #     kmeans.fit(embeddings)
                #     labels = kmeans.labels_
                #     # st.session_state.model_df["Cluster"] = labels

                #     # Change the random state for t-SNE
                #     tsne = TSNE(n_components=3, perplexity=15, random_state=41, init="random", learning_rate=200)
                #     vis_dims3 = tsne.fit_transform(embeddings)

                #     x = [x for x, y, z in vis_dims3]
                #     y = [y for x, y, z in vis_dims3]
                #     z = [z for x, y, z in vis_dims3]

                #     # Create a figure
                #     fig = plt.figure(figsize=(25, 25))
                #     ax = fig.add_subplot(111, projection='3d')

                #     # Visualize clusters using Scatter plots
                #     for category, color in enumerate(["purple", "green", "red", "blue", "yellow", "orange", 'black', 'brown', 'grey', 'olive', 'cyan', 'pink']):
                #         xs = np.array(x)[st.session_state.model_df.cluster == category]
                #         ys = np.array(y)[st.session_state.model_df.cluster == category]
                #         zs = np.array(z)[st.session_state.model_df.cluster == category]
                #         ax.scatter(xs, ys, zs, color=color, alpha=0.3, label=cluster_name)

                #         avg_x = xs.mean()
                #         avg_y = ys.mean()
                #         avg_z = zs.mean()

                #         ax.scatter(avg_x, avg_y, avg_z, marker="x", color=color, s=100)
                #         # Annotate with the cluster name
                #         cluster_name = cluster_name_dict.get(category, f"Cluster {category}")  # Default to "Cluster {category}" if not found
                #         ax.text(avg_x, avg_y, avg_z, cluster_name, fontsize=16)
                #     ax.legend(title="Clusters")
                #     ax.set_title("Clusters identified visualized in language 3D using t-SNE")

                #     # Display the plot using Streamlit
                #     st.pyplot(fig)
                #     size_and_cluster_number = pd.DataFrame(st.session_state.model_df.groupby("cluster_name").size().sort_values(ascending=False)).reset_index()
                #     size_and_cluster_number
                #     # print out 10 messages from n cluster
                #     for index, row in size_and_cluster_number.iterrows():
                #         cluster_name = row['cluster_name']
                #         cluster_size = row[0]
                #         st.write(f"The name of the cluster is {cluster_name} and its size is {cluster_size} complaints.")
                #         st.write(st.session_state.model_df[st.session_state.model_df.cluster_name == cluster_name].sample(10)[selected_cols_topic_modelling[i]].values)
                #         st.write("\n")

                # i=i+1  

        if "model_df" in st.session_state:
            if "df_viz" not in st.session_state:
                    st.session_state.df_viz=st.session_state.model_df

            if "df_viz" in st.session_state:
                    st.session_state.df_viz=st.session_state.model_df   
            csv = convert_df(st.session_state.model_df)
            st.download_button(
                label="Download data as CSV",
                data=csv,
                file_name='topic_model_data.csv',
                mime='text/csv',
                ) 
                         
elif options =='Summarization':
    if "model_df" in st.session_state:
        from langchain.text_splitter import CharacterTextSplitter
        from langchain.text_splitter import RecursiveCharacterTextSplitter
        from summarizer import ClusterSummarizer
        selected_cluster = st.multiselect("Select a Cluster", st.session_state.model_df['cluster_name'].unique().tolist())
        if st.button("Run Summarization"):
            result_df=pd.DataFrame(st.session_state.model_df)
            if "selected_cols_topic_modelling" in st.session_state :
                selected_cols_topic_modelling=st.session_state.selected_cols_topic_modelling
            if "selected_cols_topic_modelling" not in st.session_state :
                selected_cols_topic_modelling= result_df["PREPROCESSED_TEXT"]
            # Assuming 'cluster_name' is the column you want to filter on
            result_df = result_df[result_df['cluster_name'].isin(selected_cluster)]
            # result_df=result_df[result_df[selected_cluster]==0]
            st.write(result_df)
            from langchain import PromptTemplate
            from langchain.chat_models import ChatOpenAI
            from langchain import OpenAI, PromptTemplate, LLMChain
            from langchain.chat_models import ChatOpenAI
            map_template_string = """The following is a list of reviews
            {reviews}

            Based on this list of reviews, please do 3 things: 
            (1) Identify the most recurring and significant theme beyond just general information.
            (2) Give a list of main sub-themes
            (3) Give a representitive example complaints in each sub-theme
            (4) estimate the proportion of reviews that fall into each sub-theme

            Helpful Answer:"""

            reduce_template_string = """
            The following is a list of summaries generated from the last step:
            {summaries}

            Take these and distill it into a final, consolidated list with: 
            (1) The detailed description of main theme. Only respond with facts relevant to the summary to understand the underlying information. 
            (2) Four to five representative example reviews related to main themes.
            (3) detailed paragraph for each sub-theme present in summaries

            Helpful Answer:"""

            MAP_PROMPT = PromptTemplate(input_variables=["reviews"], template=map_template_string)
            REDUCE_PROMPT = PromptTemplate(input_variables=["summaries"], template=reduce_template_string) 
            summarizer = ClusterSummarizer(result_df)
            summarizer.process_clusters(selected_cols_topic_modelling)

            # Run summarization
            results = summarizer.run_summarization(MAP_PROMPT, REDUCE_PROMPT)

            # Display results
            for result in results:
                st.write(result)
                st.write("\n===============================================================\n\n")

        # selected_cluster = st.multiselect("Select a Cluster", st.session_state.model_df['cluster_name'].unique().tolist())
        # i=0
        # while (i < len(selected_cluster)):
        #     if (i >= len(selected_cluster)):
        #             break
        #     def create_doc(messages, max_tokens=15000):
        #         # Remove duplicate messages
        #         unique_messages = list(set(messages))
        #         #input_doc = '\n\n'.join(messages)
        #         input_doc = '\n\n'.join(unique_messages)

        #         text_splitter = RecursiveCharacterTextSplitter(separators=["\n\n", "\n"], chunk_size=max_tokens, chunk_overlap=500)
        #         docs = text_splitter.create_documents([input_doc])
        #         num_docs = len(docs)
        #         num_tokens_first_doc = len(docs[0].page_content)

        #         return docs, num_docs, num_tokens_first_doc

        #     docs_list = []

        #     size_and_cluster_number = pd.DataFrame(st.session_state.model_df.groupby("cluster_name").size().sort_values(ascending=False)).reset_index()

        #     for index, row in size_and_cluster_number.iterrows():
        #         cluster_name = row['cluster_name']
        #         cluster_size = row[0]


        #         if cluster_name == selected_cluster[i]:
        #             st.write(f"Cluster {cluster_name} has {cluster_size} messages")
        #             messages = st.session_state.model_df[st.session_state.model_df.cluster_name == cluster_name]["PREPROCESSED_TEXT"].values
        #             doc, num_docs, num_tokens_first_doc = create_doc(messages)
        #             st.write(f"Now we have {num_docs} documents and the first one has {num_tokens_first_doc} tokens")
        #             st.write("\n\n")
        #             docs_list.append(doc) 
        #     from langchain import PromptTemplate
        #     from langchain.chat_models import ChatOpenAI
        #     from langchain import OpenAI, PromptTemplate, LLMChain
        #     from langchain.chat_models import ChatOpenAI
        #     map_template_string = """The following is a list of reviews
        #     {reviews}

        #     Based on this list of reviews, please do 3 things: 
        #     (1) Identify the most recurring and significant theme beyond just general information.
        #     (2) Give a list of main sub-themes
        #     (3) Give a representitive example complaints in each sub-theme
        #     (4) estimate the proportion of reviews that fall into each sub-theme

        #     Helpful Answer:"""

        #     reduce_template_string = """
        #     The following is a list of summaries generated from the last step:
        #     {summaries}

        #     Take these and distill it into a final, consolidated list with: 
        #     (1) The detailed description of main theme. Only respond with facts relevant to the summary to understand the underlying information. 
        #     (2) Four to five representative example reviews related to main themes.
        #     (3) detailed paragraph for each sub-theme present in summaries

        #     Helpful Answer:"""

        #     MAP_PROMPT = PromptTemplate(input_variables=["reviews"], template=map_template_string)
        #     REDUCE_PROMPT = PromptTemplate(input_variables=["summaries"], template=reduce_template_string) 

        #     def run_map(input_doc, llm_chain):
        #         return llm_chain.run(reviews=input_doc)
            
        #     llm = ChatOpenAI(openai_api_key=openai.api_key, temperature=0, model_name="gpt-3.5-turbo-16k")
        #     map_llm_chain = LLMChain(llm=llm, prompt=MAP_PROMPT) 
        #     initial_results = []

        #     # Iterate through docs_list and input_doc
        #     for sub_list in docs_list:
        #         for input_doc in sub_list:
        #             input_data = {"reviews": input_doc.page_content}
        #             initial_result = run_map(input_data, map_llm_chain)  # Run the model for the current input
        #             initial_results.append(initial_result)
        #     #initial_results = [run_map(input_doc.page_content) for sub_list in docs_list for input_doc in sub_list]
        #     # for k, result in enumerate(tqdm(initial_results[:len(size_and_cluster_number)])):
        #     #     st.write(f"Cluster {size_and_cluster_number.iloc[k].values[0]} has {size_and_cluster_number.iloc[k].values[1]} complaints: \n")
        #     #     st.write(result)
        #     #     st.write("\n===============================================================\n\n")  
        #     if selected_cluster:
        #         for cluster_index, cluster_name in enumerate(size_and_cluster_number['cluster_name']):
        #             if cluster_name in selected_cluster:
        #                 st.write(f"Cluster {cluster_name} has {size_and_cluster_number.iloc[cluster_index].values[1]} complaints: \n")
        #                 # st.write(initial_results[cluster_index].values[1])
        #                 st.write(initial_results[0])
        #                 st.write("\n===============================================================\n\n")
        #     else:
        #         # Handle the case when no cluster is selected
        #         st.write("Please select a cluster.") 

        #     import warnings
        #     warnings.filterwarnings("ignore", message="engine is not default parameter")
        #     from tqdm import tqdm
        #     from langchain.chains.mapreduce import MapReduceChain
        #     from langchain.text_splitter import CharacterTextSplitter
        #     from langchain.chains import (
        #                     StuffDocumentsChain,
        #                     LLMChain,
        #                     ReduceDocumentsChain,
        #                     MapReduceDocumentsChain,
        #                 )
        #     def run_mr(input_doc,MAP_PROMPT,REDUCE_PROMPT):


        #         llm_map = ChatOpenAI(openai_api_key=openai.api_key, temperature=0, model_name="gpt-3.5-turbo-16k")
        #         map_llm_chain = LLMChain(llm=llm_map, prompt=MAP_PROMPT)

        #         llm_reduce = ChatOpenAI(openai_api_key=openai.api_key, temperature=0, model_name="gpt-3.5-turbo-16k")
        #         reduce_llm_chain = LLMChain(llm=llm_reduce, prompt=REDUCE_PROMPT)

        #         combine_documents_chain = StuffDocumentsChain(
        #             llm_chain=reduce_llm_chain, 
        #             document_variable_name="summaries"
        #         )

        #         reduce_documents_chain = ReduceDocumentsChain(
        #             combine_documents_chain=combine_documents_chain,
        #             collapse_documents_chain=combine_documents_chain,
        #             token_max=15000
        #         )

        #         combine_documents = MapReduceDocumentsChain(
        #             llm_chain=map_llm_chain,
        #             reduce_documents_chain=reduce_documents_chain,
        #             document_variable_name="reviews",
        #             return_intermediate_steps=False
        #         )

        #         text_splitter = CharacterTextSplitter.from_tiktoken_encoder(
        #             chunk_size=15000,
        #             chunk_overlap=500,
        #             separator="\n\n"
        #         )

        #         map_reduce = MapReduceChain(
        #             combine_documents_chain=combine_documents,
        #             text_splitter=text_splitter
        #         )
                
        #         return map_reduce.run(input_text=input_doc)
            
        #     initial_results_1 = [run_mr(input_doc.page_content, MAP_PROMPT, REDUCE_PROMPT) for sub_list in docs_list for input_doc in sub_list]

        #     # for i, result in enumerate(tqdm(initial_results[:len(size_and_cluster_number)])):
        #     #     st.write(f"Cluster {size_and_cluster_number.iloc[i].values[0]} has {size_and_cluster_number.iloc[i].values[1]} complaints: \n")
        #     #     st.write(result)
        #     if selected_cluster:
        #         for cluster_index, cluster_name in enumerate(size_and_cluster_number['cluster_name']):
        #             if cluster_name in selected_cluster:
        #                 st.write(f"Cluster {cluster_name} has {size_and_cluster_number.iloc[cluster_index].values[1]} complaints: \n")
        #                 st.write(initial_results_1[0])
        #                 st.write("\n===============================================================\n\n")    
        #     i=i+1

elif options == 'Semantic Search':
    from sklearn.metrics.pairwise import cosine_similarity
    from sentence_transformers import SentenceTransformer

# Load the SentenceTransformer model outside the function to avoid repeated loading
    def semantic_search(semantic_search_col_2, model_choice, sementic_search_df, semantic_search_col):
        # if model_choice not in ['MiniLM', 'DistilRoBERTa', 'RoBERTa','Ada']:
        #     st.error("Invalid model choice. Please choose from 'MiniLM', 'DistilRoBERTa', 'RoBERTa' Or 'Ada")
        #     return None
        if 'Column-to-Column Comparison' in semantic_search_choice :
            if model_choice == 'Ada':
                def get_embeddings_batch(texts_to_embed):
                                            # Embed the lines of text as a batch
                                            response = openai.Embedding.create(
                                                model="text-embedding-ada-002",
                                                input=texts_to_embed
                                            )
                                            # Extract the AI output embeddings as a list of lists of floats
                                            embeddings_batch = response["data"]
                                            embeddings = [item["embedding"] for item in embeddings_batch]
                                            return embeddings
                text_to_embed_list = sementic_search_df[semantic_search_col[0]].astype(str).tolist()
                text_to_embed_list_2 = sementic_search_df[semantic_search_col_2[0]].astype(str).tolist()
            
            # Get embeddings for all reviews
                review_embeddings = get_embeddings_batch(text_to_embed_list)
                review_embeddings_2 = get_embeddings_batch(text_to_embed_list_2)

                
            else:
                # Compute embeddings for the reviews
                review_embeddings = model.encode(sementic_search_df[semantic_search_col[0]].astype(str))
                review_embeddings_2 = model.encode(sementic_search_df[semantic_search_col_2[0]].tolist())
                 
            # Calculate the similarities
            similarities = [cosine_similarity([emb1], [emb2])[0][0] for emb1, emb2 in zip(review_embeddings, review_embeddings_2)]
            # Add the similarities to the DataFrame
            sementic_search_df['similarity'] = similarities
            # Sort the DataFrame by the similarity scores
            sementic_search_df = sementic_search_df.sort_values(by='similarity', ascending=False)
            return sementic_search_df
        
        elif 'Query-Based Comparison' in semantic_search_choice :      
            if model_choice == 'Ada':
                def get_embeddings_batch(texts_to_embed):
                                                # Embed the lines of text as a batch
                                                response = openai.Embedding.create(
                                                    model="text-embedding-ada-002",
                                                    input=texts_to_embed
                                                )
                                                # Extract the AI output embeddings as a list of lists of floats
                                                embeddings_batch = response["data"]
                                                embeddings = [item["embedding"] for item in embeddings_batch]
                                                return embeddings
                text_to_embed_list = sementic_search_df[semantic_search_col[0]].astype(str).tolist()
                query_list = [query]  # Wrap the query in a list if it's a single query
            
            # Get embeddings for all reviews
                review_embeddings = get_embeddings_batch(text_to_embed_list)
            
            # Get embedding for the query
                query_embedding = get_embeddings_batch(query_list)[0]
                
            else:
                # Compute embeddings for the reviews
                review_embeddings = model.encode(sementic_search_df[semantic_search_col[0]].astype(str))
                
                # Compute the embedding for the query
                query_embedding = model.encode([query])[0]  
            # Calculate the similarities
            similarities = cosine_similarity([query_embedding], review_embeddings)[0]
            # Add the similarities to the DataFrame
            sementic_search_df['similarity'] = similarities
            # Sort the DataFrame by the similarity scores
            sementic_search_df = sementic_search_df.sort_values(by='similarity', ascending=False)
            return sementic_search_df

    if "pre_process_df" in st.session_state:
        sementic_search_df = st.session_state.pre_process_df  
        semantic_search_choice = st.multiselect(label="Select the Mode",options=['Column-to-Column Comparison','Query-Based Comparison'],key="Semantic Search Mode")
        if 'Column-to-Column Comparison' in semantic_search_choice :
                    semantic_search_col = st.multiselect(label="Select First Column for cosine similarity", options=sementic_search_df.columns, key="Semantic Search")
                    semantic_search_col_2 = st.multiselect(label="Select First Column for cosine similarity", options=sementic_search_df.columns, key="Semantic Search 2")
                    model_choice = st.selectbox(label="Select Model Choice for Query", options=['MiniLM', 'DistilRoBERTa', 'RoBERTa','Ada'])
                    if  model_choice  in ['MiniLM', 'DistilRoBERTa', 'RoBERTa']:
                        model = SentenceTransformer('paraphrase-MiniLM-L6-v2')
                    if st.button("Run Semantic Search"):
                        if len(semantic_search_col_2) > 0:
                            df_sorted = semantic_search(semantic_search_col_2,model_choice, sementic_search_df, semantic_search_col)
                            st.write(df_sorted)
                        else:
                            st.error("Please Select Both the columns  before running the semantic search.")

        elif 'Query-Based Comparison' in semantic_search_choice :     
                    semantic_search_col = st.multiselect(label="Select Column for cosine similarity", options=sementic_search_df.columns, key="Semantic Search")
                    query = st.text_input(label="Write Your Query")
                    model_choice = st.selectbox(label="Select Model Choice for Query", options=['MiniLM', 'DistilRoBERTa', 'RoBERTa','Ada'])
                    if  model_choice  in ['MiniLM', 'DistilRoBERTa', 'RoBERTa']:
                        model = SentenceTransformer('paraphrase-MiniLM-L6-v2')
                    if st.button("Run Semantic Search"):
                        if len(query) > 0:
                            df_sorted = semantic_search(query, model_choice, sementic_search_df, semantic_search_col)
                            st.write(df_sorted)
                        else:
                            st.error("Please enter a query before running the semantic search.")

    else:
        st.error("Please Run Text Preprocessing Module First")

        
elif options == 'EDA' :
    if uploaded_file is not None or st.session_state['button'] == True:  
        dashboard()
    else :
         st.header('Load Your data to get started')

