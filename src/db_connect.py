import sqlalchemy as sa
import urllib
import pyodbc
import pandas as pd
import streamlit as st
from sqlalchemy.orm import scoped_session,sessionmaker
from sqlalchemy import create_engine,inspect


# user = 'adityasingh'
# password = 'India111'
# host = 'adityatemp.database.windows.net'
# port = 1433
# database = 'temp'
# driver = 'SQL Server'
def db_connect():
    st.sidebar.markdown("<h1 style='text-align: center; color: red;'>Connect To SQL DB!!</h1>", unsafe_allow_html=True)
    user = st.sidebar.text_input("Username")
    password = st.sidebar.text_input("Password",type="password")
    host = st.sidebar.text_input("Host/Server")
    port = st.sidebar.text_input("Port")
    database = st.sidebar.text_input("Database")
    # driver = 'SQL Server'
    # button = st.sidebar.button("Connect",key='Click to connect')
    # st.write(button)
    def auth():
        conn= urllib.parse.quote_plus('DRIVER={SQL Server};SERVER='+host+';DATABASE='+database+';UID='+user+';PWD='+ password)
        engine = sa.create_engine('mssql+pyodbc:///?odbc_connect={}'.format(conn),use_setinputsizes=False)
        st.write(
                    f"Connection to the {host} for user {user} created successfully.")
        inspection = inspect(engine)
        all_tables = inspection.get_table_names()
        # all_tables1 = ' ', inspection.get_table_names()
        # st.write(all_tables1)
        tablename = st.selectbox("Select Table Name",all_tables)
        df2=pd.read_sql_table(table_name=tablename,con=engine)
        st.write(df2)
        print(df2)



    if st.sidebar.button("Connect"):
        auth()

button1 = st.button("Connect to Database")

if st.session_state.get('button') != True:
    st.session_state['button'] = button1

if st.session_state['button'] == True:
    db_connect()


    # db=scoped_session(sessionmaker(bind=engine))
    # df = pd.read_excel(r'C:\Users\qahmed\Documents\test\Review_Data_Final.xlsx')
    # a=df.to_sql("new_table",con=engine,if_exists='replace')
    # print(a)
    # from sqlalchemy.sql import text
    # usernamedata=db.execute(text('SELECT * FROM new_table')).fetchone()
    # print(usernamedata)
    