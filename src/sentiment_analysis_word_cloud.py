from nltk.corpus import stopwords
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
#from nltk.sentiment.vader import SentimentIntensityAnalyser
from wordcloud import WordCloud, get_single_color_func
import matplotlib.pyplot as plt

class class_sentiment_word_cloud:
    def RemoveDuplicate(list_of_words):
        #duplicate removing function
        final_list = []
        for word in list_of_words:
            if word not in final_list:
                final_list.append(word)
        return final_list
    
    def positive_lines(cleaned_lines, positive_words, negative_words):
        for key,value in cleaned_lines.items():
            if(value > 0):
                positive_words.append(key)
            else:
                negative_words.append(key)

    def inverted_lines(cleaned_lines, positive_words, negative_words):
        for key,value in cleaned_lines.items():
            if(value > 0):
                negative_words.append(key)
            else:
                positive_words.append(key)

    class AssignColor(object):
        def __init__(self, color_words_dict, default):
            self.color_words_dict = [
                (get_single_color_func(color), set(words))
                for (color, words) in color_words_dict.items()]
            
            self.default = get_single_color_func(default)
            
        def get_color(self, word):
            try:
                color = next(
                color for (color, words) in self.color_words_dict
                    if word in words)
            except StopIteration:
                color = self.default
            return color

        def __call__ (self, word, **kwargs):
            return self.get_color(word)(word, **kwargs)
        
        def out_put_word_cloud(word_cloud_df, review_list):
            phrases = review_list
            stop_words = set(stopwords.words("english"))
            # clean_string = phrases.replace(',', '')
            # words = clean_string.split()
            words = class_sentiment_word_cloud.RemoveDuplicate(stop_words)
            cleaned_lines = dict()
            positive_words = []
            negative_words = []

            sentiment_list = word_cloud_df['similarity_score'].to_list()

            i = 0
            for phrase in phrases:
                    sentiment = sentiment_list[i]
                    i+=1
                    if(sentiment != 0 ):
                        cleaned_lines[phrase] = sentiment
                        
            # print(cleaned_lines)
            # print(type(cleaned_lines))

            for key,value in cleaned_lines.items():
                    if(value > 0):
                        positive_words.append(key)
                    else:
                        negative_words.append(key)

            # if(positive_plot == 'positive'):
            #     for key,value in cleaned_lines.items():
            #         if(value > 0):
            #             positive_words.append(key)
            #         else:
            #             negative_words.append(key)
            # else:
            #     for key,value in cleaned_lines.items():
            #         if(value > 0):
            #             positive_words.append(key)
            #         else:
            #             negative_words.append(key)

                    
            # print(positive_words)
            # print(negative_words)

            color_words_dict = {
                'green': positive_words,
                'red': negative_words
            }

            # print(color_words_dict)
            #wc = WordCloud(collocations=True, background_color='white', max_words=2000).fit_words(cleaned_lines)
            wc = WordCloud(collocations=True, background_color='white' ,max_words=2000).fit_words(cleaned_lines)
            grouped_color_func = class_sentiment_word_cloud.AssignColor(color_words_dict, 'grey')
            wc.recolor(color_func=grouped_color_func)

            # plt.figure(figsize=[200,10])
            # plt.imshow(wc,interpolation='bilinear')
            # plt.axis("off")
            
            return wc