import matplotlib.pyplot as plt
import pandas as pd
import streamlit as st

class ClusterTrendVisualizer:
    def __init__(self, df,selected_cols_topic_modelling):
        self.df = df

    def plot_overall_cluster_distribution(self):
        # Calculate the size of each cluster
        cluster_sizes = self.df['cluster_name'].value_counts()

        # Plotting the pie chart for overall cluster distribution
        fig=plt.figure(figsize=(10, 8))
        plt.pie(cluster_sizes, labels=cluster_sizes.index, autopct='%1.1f%%', startangle=140)
        plt.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
        plt.title('Overall Distribution of Review Among Clusters')
        #plt.show()
        st.pyplot(fig)

    def plot_trend_for_each_cluster(self,selected_cols_topic_modelling,select_date_col):
        # Assuming 'date_column' is the name of your date column and it's in 'YYYY-MM-DD' format
        self.df[select_date_col[0]] = pd.to_datetime(self.df[select_date_col[0]])

        # Create a trend plot for each cluster
        for cluster_name in self.df['cluster_name'].unique():
            # Filter the DataFrame for the current cluster
            cluster_data = self.df[self.df['cluster_name'] == cluster_name]
            
            # Group by date and count the number of complaints
            trend_data = cluster_data.groupby(cluster_data[select_date_col[0]].dt.to_period('M')).size()
            
            # Reset index to convert PeriodIndex to DateTimeIndex for plotting
            trend_data.index = trend_data.index.to_timestamp()
            
            # Plotting
            fig=plt.figure(figsize=(10, 5))
            trend_data.plot(title=f'Trend for {cluster_name}')
            plt.ylabel('Number of review')
            plt.xlabel('Date')
            #plt.show()
            st.pyplot(fig)
            
            # Display cluster information
            cluster_size = cluster_data.shape[0]
            st.write(f"The name of the cluster is {cluster_name} and its size is {cluster_size} review.")
            if "PREPROCESSED_TEXT" in st.session_state.model_df:
                sample_complaints = cluster_data.sample(5)['PREPROCESSED_TEXT'].values
            else:
                sample_complaints = cluster_data.sample(5)[selected_cols_topic_modelling[0]].values    
            st.write(f" Some sample review from this cluster: \n {sample_complaints}")
            st.write("\n")