from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain import PromptTemplate, LLMChain
from langchain.chains.mapreduce import MapReduceChain
from langchain.chains import (
    StuffDocumentsChain,
    ReduceDocumentsChain,
    MapReduceDocumentsChain,
)
from langchain.chat_models import ChatOpenAI
#from langchain.chat_models import AzureChatOpenAI
import pandas as pd
import os
import openai
import streamlit as st
from tqdm import tqdm

class ClusterSummarizer:
    def __init__(self, df, max_tokens=15000):
        self.df = df
        self.max_tokens = max_tokens
        self.docs_list = []
        #self.initialize_env_vars()

    # def initialize_env_vars(self):
    #     os.environ["AZURE_OPENAI_API_KEY"] = "a12ad6fee4444b3e9350373c3be045ff"
    #     os.environ["AZURE_OPENAI_ENDPOINT"] = "https://cog-openai-dto-minority-dev.openai.azure.com/"

    def create_doc(self, messages):
        unique_messages = list(set(messages))
        input_doc = '\n\n'.join(unique_messages)
        text_splitter = RecursiveCharacterTextSplitter(separators=["\n\n", "\n"], chunk_size=self.max_tokens, chunk_overlap=500)
        docs = text_splitter.create_documents([input_doc])
        num_docs = len(docs)
        num_tokens_first_doc = len(docs[0].page_content) if docs else 0
        return docs, num_docs, num_tokens_first_doc

    def process_clusters(self,selected_cols_topic_modelling):
        unique_clusters = self.df['cluster_name'].unique()

        for cluster_name in unique_clusters:
            st.write(f"Processing cluster: {cluster_name}")
            if "PREPROCESSED_TEXT" in st.session_state.model_df:
                messages = self.df[self.df['cluster_name'] == cluster_name]['PREPROCESSED_TEXT'].values
            else :
                messages = self.df[self.df['cluster_name'] == cluster_name][selected_cols_topic_modelling[0]].values    
            docs, num_docs, num_tokens_first_doc = self.create_doc(messages)
            st.write(f"Cluster {cluster_name} has {num_docs} documents with {num_tokens_first_doc} tokens in the first document")
            self.docs_list.append(docs)

    def run_mr(self, input_doc, MAP_PROMPT, REDUCE_PROMPT):
        llm_map = ChatOpenAI(openai_api_key=openai.api_key, temperature=0, model_name="gpt-3.5-turbo-16k")
        map_llm_chain = LLMChain(llm=llm_map, prompt=MAP_PROMPT)

        llm_reduce = ChatOpenAI(openai_api_key=openai.api_key, temperature=0, model_name="gpt-3.5-turbo-16k")
        reduce_llm_chain = LLMChain(llm=llm_reduce, prompt=REDUCE_PROMPT)

        combine_documents_chain = StuffDocumentsChain(
            llm_chain=reduce_llm_chain, 
            document_variable_name="summaries"
        )

        reduce_documents_chain = ReduceDocumentsChain(
            combine_documents_chain=combine_documents_chain,
            collapse_documents_chain=combine_documents_chain,
            token_max=15000
        )

        combine_documents = MapReduceDocumentsChain(
            llm_chain=map_llm_chain,
            reduce_documents_chain=reduce_documents_chain,
            document_variable_name="reviews",
            return_intermediate_steps=False
        )

        text_splitter = RecursiveCharacterTextSplitter(
            separators=["\n\n", "\n"], 
            chunk_size=15000, 
            chunk_overlap=100
        )

        map_reduce = MapReduceChain(
            combine_documents_chain=combine_documents,
            text_splitter=text_splitter
        )
        
        return map_reduce.run(input_text=input_doc)


    def run_summarization(self, MAP_PROMPT, REDUCE_PROMPT):
        initial_results = []
        for sub_list in self.docs_list:
            # Combine all documents in a cluster into one large document
            combined_doc = "\n\n".join([doc.page_content for doc in sub_list])
            # Run map-reduce on the combined document
            result = self.run_mr(combined_doc, MAP_PROMPT, REDUCE_PROMPT)
            initial_results.append(result)
        return initial_results