#Keybert Algorithm imports
import pandas as pd
import nltk
from keybert import KeyBERT
import warnings
nltk.download('stopwords')
nltk.download('punkt')
warnings.simplefilter(action='ignore', category=FutureWarning)


#Key Phrase Extraction
class class_key_phrase_extraction:
        def key_bert(new_df, col_name):
                #lower_range = int(input("Enter the Lower_range for grams: ")) use_maxsum=True,nr_candidates=20
                #upper_range = int(input("Enter the Upper_range for grams: "))

                kw_model = KeyBERT()
                keywords = kw_model.extract_keywords(docs=new_df[col_name], keyphrase_ngram_range=(1,5))
                return pd.DataFrame(keywords)
        
        def explode_keybert_df(key_phrase_score_df):
                explode_df = key_phrase_score_df.apply(lambda ds: ds.map(lambda x: x if x != None else ("", 0)))
                return explode_df.explode([0,1,2,3,4])
        
        def resetting_and_adding_index_col_to_df(exploded_df):
                exploded_resetIndex_df  = exploded_df.reset_index()
                exploded_resetIndex_df  = exploded_resetIndex_df.drop('index', axis=1)
                exploded_resetIndex_df['Id'] = exploded_resetIndex_df.index
                return exploded_resetIndex_df
        
        def separating_keyphrase_rows_exploded_df(exploded_resetIndex_df):
                keyPhrase_even_exploded_df = exploded_resetIndex_df[::2]
                return keyPhrase_even_exploded_df
        
        def separating_keyphrase_rows_exploded_df_1(exploded_resetIndex_df):
                keyPhrase_even_exploded_df_1 = exploded_resetIndex_df[2::2]
                return keyPhrase_even_exploded_df_1
        
        def separating_SimilarityScore_rows_exploded_df(exploded_resetIndex_df):
                similarityScore_odd_exploded_df = exploded_resetIndex_df[1::2]
                return similarityScore_odd_exploded_df
        
        def best_key_phrase_index(max_score, scores):
                min = abs(max_score-scores[0])
                min_ind = 0
                curr_ind = 0
                for score in scores:
                        if min > abs(max_score - score):
                                min = abs(max_score - score)
                                min_ind = curr_ind
                                curr_ind += 1
                return min_ind
        
        def get_key_phrase_score_df(sentiment_scores, similarity_scores, key_phrase_list):
                sentiment_score_list = []
                key_phrase_score_list = []
                similarity_score_list = []
                
                i = 0
                for score in sentiment_scores:
                        best_phrase_index = class_key_phrase_extraction.best_key_phrase_index(1, similarity_scores[i])

                        key_phrase_score_list.append(key_phrase_list[i][best_phrase_index])
                        similarity_score_list.append(similarity_scores[i][best_phrase_index])
                        sentiment_score_list.append(sentiment_scores[i])
                        i+=1
                
                sentiment_df = pd.DataFrame(sentiment_score_list)
                key_phrase_df = pd.DataFrame(key_phrase_score_list)
                similarity_df = pd.DataFrame(similarity_score_list)
                test = pd.concat([sentiment_df, key_phrase_df, similarity_df], axis=1)
                test.columns = ['sentiment_final_score', 'key_phrase','similarity_score']
                return test
        
        def finalize_key_phrase(key_phrase_df):
                exploded_similarity_score_df = class_key_phrase_extraction.separating_SimilarityScore_rows_exploded_df(key_phrase_df)
                exploded_key_phrase_df = class_key_phrase_extraction.separating_keyphrase_rows_exploded_df(key_phrase_df)
         
                sentiment_scores = exploded_key_phrase_df['sentiment_final_score'].to_list()
                similarity_scores = exploded_similarity_score_df[[0,1,2, 3, 4]].values.tolist()
                key_phrase_list = exploded_key_phrase_df[[0, 1, 2, 3, 4]].values.tolist()
                
                final_key_phrase_df = class_key_phrase_extraction.get_key_phrase_score_df(sentiment_scores, similarity_scores, key_phrase_list)
                
                return final_key_phrase_df
        
        def output_keyPhrase_similarityScore_df_to_user(word_cloud_df, key_phrase_df):
                user_output_df = word_cloud_df
                user_output_df['key_phrase_and_similarity_score'] = user_output_df[['key_phrase', 'sentiment_final_score']].apply(tuple, axis=1)
                user_consumable_df = pd.concat([key_phrase_df,user_output_df], axis = 1)
                
                # usr_df = user_consumable_df.drop([0,1,2,3,4,'flair_sentiment','flair_score','vader_score','vader_sentiment','textBlob_score','TextBlob_sentiment','roberta_sentiment','roberta_score','sentiment_mean','sentiment_final_score','key_phrase','similarity_score','inverted_sentiment_score'], axis = 1)
                usr_df = user_consumable_df.drop([0,1,2,3,4, 'sentiment_final_score','key_phrase'], axis = 1)
                final_user_consumable_df = usr_df.explode(['key_phrase_and_similarity_score'])
                return final_user_consumable_df


