#Flair Sentiment Analysis imports
import pandas as pd
from flair.models import TextClassifier as classifier
from flair.data import Sentence as Sentence

## Flair Sentiment Analysis
class class_flair_sentiment_analysis:
    def flair_sentiment_analysis(df, col_name):
        sentiment=[]
        score=[]

        sentiment_model = classifier.load('en-sentiment')

        for sentence in df[col_name]:
            if sentence.strip()=="":
                sentiment.append("")
                score.append(0.5)
            else:
                curr_flair_sentence = Sentence(sentence)
                sentiment_model.predict(curr_flair_sentence)
                sentiment.append(curr_flair_sentence.labels[0].value)
                score.append(curr_flair_sentence.labels[0].score)

        df['flair_sentiment']=sentiment
        df['flair_score']=score
        return df
