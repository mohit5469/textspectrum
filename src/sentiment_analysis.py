import pandas as pd

import sentiment_analysis_vader
import sentiment_analysis_text_blob
import sentiment_analysis_roberta
import sentiment_analysis_flair
import sentiment_analysis_sentiment_score_final
import sentiment_analysis_key_phrase_extraction_using_key_bert
import sentiment_analysis_word_cloud

# Read in data
#df = pd.read_csv(r'C:\Users\qahmed\Documents\test\vcreatek_db\sample_data.csv', encoding='latin-1')
df = pd.read_csv(r'sentiment_V5\sample_data.csv', encoding='latin-1')
df = df.fillna('')

col_name = 'REVIEW_CONTENT'
sentiment_analysis_vader.class_vader_sentiment_analysis.vader_sentiment_analysis(df, col_name)
sentiment_analysis_text_blob.text_blob_sentiment_analysis.text_blob_sentiment_analysis(df, col_name)
sentiment_analysis_roberta.class_roberta_sentiment_analysis.roberta_sentiment_analysis(df, col_name)
sentiment_analysis_flair.class_flair_sentiment_analysis.flair_sentiment_analysis(df, col_name)
# print(df)
sentiment_df = sentiment_analysis_sentiment_score_final.class_sentiment_score.finalize_sentiment_score(df)
df['sentiment'] = sentiment_df['sentiment_final_score'].apply(sentiment_analysis_sentiment_score_final.class_sentiment_score.getAnalysis)
# print(df)
#key phrase from
key_phrase_df = sentiment_analysis_key_phrase_extraction_using_key_bert.class_key_phrase_extraction.key_bert(df.reset_index(), 'REVIEW_CONTENT')
key_phrase_df['sentiment_final_score']=df['sentiment']
exploded_df = sentiment_analysis_key_phrase_extraction_using_key_bert.class_key_phrase_extraction.explode_keybert_df(key_phrase_df)
exploded_reset_index_df = sentiment_analysis_key_phrase_extraction_using_key_bert.class_key_phrase_extraction.resetting_and_adding_index_col_to_df(exploded_df)

phrase_df = sentiment_analysis_key_phrase_extraction_using_key_bert.class_key_phrase_extraction.separating_keyphrase_rows_exploded_df(exploded_reset_index_df)
print("++++++++++++++++++++++++++++++++++")
print(exploded_df.columns)
similarity_score = sentiment_analysis_key_phrase_extraction_using_key_bert.class_key_phrase_extraction.separating_SimilarityScore_rows_exploded_df(exploded_reset_index_df)

positive_phrase_df = sentiment_analysis_key_phrase_extraction_using_key_bert.class_key_phrase_extraction.finalize_key_phrase(exploded_df)
#word_cloud_df = sentiment_analysis_key_phrase_extraction_using_key_bert.class_key_phrase_extraction.inverted_sentiment_score(positive_phrase_df, key_phrase_df)
user_output_df = sentiment_analysis_key_phrase_extraction_using_key_bert.class_key_phrase_extraction.output_keyPhrase_similarityScore_df_to_user(positive_phrase_df,key_phrase_df) 
print("==============================++++================")
print(positive_phrase_df['similarity_score'])

positive_plot = sentiment_analysis_word_cloud.class_sentiment_word_cloud.AssignColor.out_put_word_cloud(positive_phrase_df, positive_phrase_df['key_phrase'].values.tolist())
positive_plot.show()


# #word_cloud
# word_cloud = sentiment_analysis_word_cloud.create_word_cloud(df, df['key_phrase'].values.tolist())
# word_cloud.show()