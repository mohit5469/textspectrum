#Roberta imports
import pandas as pd
from transformers import AutoTokenizer
from transformers import AutoModelForSequenceClassification
from scipy.special import softmax

from tqdm.notebook import tqdm

#Roberta- Robustly Optimized BERT-Pretraining Approach
class class_roberta_sentiment_analysis:
	def roberta_sentiment_analysis(df, col_name):
		MODEL = f"cardiffnlp/twitter-roberta-base-sentiment"
		tokenizer = AutoTokenizer.from_pretrained(MODEL)
		model = AutoModelForSequenceClassification.from_pretrained(MODEL)
		
		def roberta_score_sentiment(scores):
			max_score = abs(scores[0]);
			max_index = 0
			currIndex = 0
			for score in scores:
				if max_score < score:
					max_score = score
					max_index = currIndex
				currIndex+=1
				
			if max_index == 0:
				sentiment = 'Negative'
			
			if max_index == 1:
				sentiment = 'Neutral'
			
			if max_index == 2:
				sentiment = 'Positive'
				
			score_dict = { "roberta_sentiment": sentiment,
						"roberta_score" : max_score}
			return score_dict

		def polarity_scores_roberta(df):
			encoded_text = tokenizer(df, return_tensors='pt')
			output = model(**encoded_text)
			scores = output[0][0].detach().numpy()
			scores = softmax(scores)
			
			return roberta_score_sentiment(scores)

		def roberta_bert(df):
			res = []
			for i in df.items():
				try:
					# print(i)
					text = i[1]
					roberta_result = polarity_scores_roberta(text)
					both = {**roberta_result}
					# print(both, "\n")
					res.append(both)
				except RuntimeError:
					print(f'Broke for id {i}')
			return res
		roberta_result_pd = pd.DataFrame(roberta_bert(df[col_name]))
		new_df = pd.concat([df, roberta_result_pd], axis=1)
		df['roberta_sentiment']=new_df['roberta_sentiment']
		df['roberta_score']=new_df['roberta_score']
		return df
	
