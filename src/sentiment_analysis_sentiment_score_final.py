import pandas as pd

#Finalize Sentiment Score
class class_sentiment_score:
    def get_sentiment_mean(new_df):     
        new_df['flair_sentiment'] = new_df['flair_sentiment'].fillna(0)
        new_df['flair_score'] = new_df['flair_score'].fillna(0)
        sentiment_mean = new_df[['flair_score', 'vader_score', 'textBlob_score', 'roberta_score']].mean(axis=1)
        new_df['sentiment_mean'] = sentiment_mean
        return new_df

    def best_score(mean, scores):
        min = abs(mean-scores[0])
        min_ind = 0
        curr_ind = 0
        for score in scores:
            if min > abs(mean - score):
                min = abs(mean - score)
                min_ind = curr_ind
            curr_ind += 1
    #     print("min_ind: ", min_ind)
    #     print("score", scores[min_ind])
        return scores[min_ind]

    def get_sentiment_score_list(means, scores):
        sentiment_scores_list = []
        i = 0
        for mean in means:
            #print(type(scores[i]))
            sentiment_scores_list.append(class_sentiment_score.best_score(mean, scores[i]))
            i+=1
        return sentiment_scores_list

    def normalize_df(df_n, col_name):
        df_n[col_name] = (df_n[col_name]/(df_n[col_name].abs().max()))

    def finalize_sentiment_score(new_df):
        df_with_sentiment_mean = class_sentiment_score.get_sentiment_mean(new_df)
        
        means = df_with_sentiment_mean['sentiment_mean'].tolist()
        scores = new_df[['flair_score','vader_score','textBlob_score']].values.tolist()
        sentiment_score_list = class_sentiment_score.get_sentiment_score_list(means, scores)
        
        df_with_sentiment_mean['sentiment_final_score'] = pd.DataFrame(sentiment_score_list)
        
        class_sentiment_score.normalize_df(new_df, 'sentiment_final_score')
        
        return df_with_sentiment_mean

    def getAnalysis(score):
        if score < 0.4:
            return 'Negative'
        elif score > 0.6:
            return 'Positive'
        else:
            return 'Neutral'