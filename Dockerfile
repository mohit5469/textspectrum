# Build Stage
FROM python:3.11.7 as builder

WORKDIR /app

COPY requirements.txt .

# Install dependencies for building
RUN apt-get update && \
    apt-get install -y unixodbc-dev && \
    pip install --no-cache-dir -r requirements.txt

# Runtime Stage
FROM python:3.11.7-slim

WORKDIR /app

# Copy only necessary files from the build stage
COPY --from=builder /usr/local/lib/python3.11/site-packages/ /usr/local/lib/python3.11/site-packages/
COPY --from=builder /usr/local/bin/ /usr/local/bin/
COPY --from=builder /app .

# Expose the port that Streamlit listens on (default is 8501)
EXPOSE 8501

# Command to run your Streamlit application
CMD ["streamlit", "run", "src/nlp_app.py"]
